package com.telbound.utils;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.telbound.adapters.MessageAdapater;
import com.telbound.models.Message;

public interface AsyncResponse {
    void processFinish(String output, String key, Message message, MessageAdapater.MessageViewHolder myMessageViewHolder);
}
