package com.telbound.utils;

import android.media.Image;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.telbound.adapters.MessageAdapater;
import com.telbound.models.Message;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

public class MyURLChecker extends AsyncTask<Void, Void, String> {
    public String urlToCheck;
    public AsyncResponse delegate = null;

    private String key;
    private Message message;
    private TextView txt_view;
    private ImageView imgView, videoIcon;
    private CardView myCardView;
    private MessageAdapater.MessageViewHolder myMessageViewHolder;

//    public MyURLChecker(String urlToCheck, String key, Message message, TextView txt_view, CardView myCardView, ImageView imgView, ImageView videoIcon) {
    public MyURLChecker(String urlToCheck, String key, Message message, MessageAdapater.MessageViewHolder myMessageViewHolder) {
        this.urlToCheck = urlToCheck;
        this.key = key;
        this.message = message;
//        this.txt_view = txt_view;
//        this.imgView = imgView;
//        this.videoIcon = videoIcon;
//        this.myCardView = myCardView;
        this.myMessageViewHolder = myMessageViewHolder;
    }

    @Override
    protected String doInBackground(Void... voids) {
//        checkUrlContentType(urlToCheck);
        return checkUrlContentType(urlToCheck);
    }

    public static String checkUrlContentType(String uri)
    {
        String contenType = "";

        URLConnection connection = null;
        try {
            connection = new URL(uri).openConnection();
            contenType = connection.getHeaderField("Content-Type");
            return  contenType;
        } catch (IOException e) {
            //e.printStackTrace();
            return contenType;
        }

    }



    @Override
    protected void onPostExecute(String result) {
//        delegate.processFinish(result, key, message, txt_view, myCardView, imgView, videoIcon);
        delegate.processFinish(result, key, message, myMessageViewHolder);
    }
}
