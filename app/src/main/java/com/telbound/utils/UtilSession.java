package com.telbound.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.telbound.orca.AuthActivity;

import java.util.HashMap;

public class UtilSession {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    Context _context;

    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "NCST-Spref";
    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String KEY_ID = "id";
    public static final String KEY_UUID = "uuid";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_NAME = "name";

    @SuppressLint("CommitPrefEdits")
    public UtilSession(Context context){
        this._context = context;
        sharedPreferences = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void login(String id, String uuid, String email, String name){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_ID, id);
        editor.putString(KEY_UUID, uuid);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_NAME, name);
        editor.commit();
    }

    public void checkLogin(){
        if(!this.isLoggedIn()){
            Intent i = new Intent(_context, AuthActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            _context.startActivity(i);
        }
    }

    public HashMap<String, String> getDetails(){
        HashMap<String, String> user = new HashMap<>();

        user.put(KEY_ID, sharedPreferences.getString(KEY_ID, null));
        user.put(KEY_UUID, sharedPreferences.getString(KEY_UUID, null));
        user.put(KEY_EMAIL, sharedPreferences.getString(KEY_EMAIL, null));
        user.put(KEY_NAME, sharedPreferences.getString(KEY_NAME, null));

        return user;
    }

    public void logout(){
        editor.clear();
        editor.commit();

        Intent i = new Intent(_context, AuthActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        _context.startActivity(i);
    }

    public boolean isLoggedIn(){
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }
}
