package com.telbound.utils;

public interface FastScrollStateChangeListenerCustom {
    /**
     * Called when fast scrolling begins
     */
    void onFastScrollStart(PixCustom fastScroller);

    /**
     * Called when fast scrolling ends
     */
    void onFastScrollStop(PixCustom fastScroller);
}
