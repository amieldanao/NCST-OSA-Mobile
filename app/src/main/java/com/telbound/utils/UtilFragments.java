package com.telbound.utils;

public class UtilFragments {

    public static final String LoginFragment = "LoginFragment";
    public static final String RecoveryFragment = "RecoveryFragment";

    // Main activity fragments
    public static final String HomeFragment = "HomeFragment";
    public static final String ReportsFragment = "ReportsFragment";
    public static final String HelpFragment = "HelpFragment";
    public static final String AlertsFragment = "AlertsFragment";
    public static final String AccountFragment = "AccountFragment";
    public static final String NotifFragment = "NotifFragment";

}
