package com.telbound.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.LruCache;

import com.bumptech.glide.load.resource.gif.GifDrawable;

import java.util.HashMap;

public class ImageCache {
    private static final String TAG = "myLogTag";
    private LruCache<String, Bitmap> memoryCache;
    public LruCache<String, Bitmap> image_links_cache;
    private HashMap<String, Uri> bitmapUriCache;
    private HashMap<String, Uri> gifMemoryUriCache;

    public static boolean isValidContextForGlide(final Context context) {
        if (context == null) {
            return false;
        }
        if (context instanceof Activity) {
            final Activity activity = (Activity) context;
            if (activity.isDestroyed() || activity.isFinishing()) {
                return false;
            }
        }
        return true;
    }

    public ImageCache()
    {
        memoryCache = createLruCache();
        image_links_cache = createLruCache();

        gifMemoryUriCache = new HashMap<>();
        bitmapUriCache = new HashMap<>();
    }

    private LruCache<String, Bitmap> createLruCache()
    {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;
        return new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        memoryCache.put(key, bitmap);
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return memoryCache.get(key);
    }

    public void addBitmapToUriCache(String key, Uri uri)
    {
        bitmapUriCache.put(key, uri);
    }

    public Uri getBitmapFromUriCache(String key)
    {
        return bitmapUriCache.get(key);
    }

    public void addGifUriToCache(String key, Uri uri)
    {
        gifMemoryUriCache.put(key, uri);
    }

    public Uri getGifUriFromCache(String key)
    {
        return  gifMemoryUriCache.get(key);
    }
}
