package com.telbound.utils;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.telbound.orca.ComposerActivity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FetchServerTimeTask extends AsyncTask<String, Void, Date> {
    final MyCallbackInterface callback;
    private FirebaseFirestore db;

    public FetchServerTimeTask(MyCallbackInterface callback) {
        this.callback = callback;
        db = FirebaseFirestore.getInstance();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();



        DocumentReference serverTimeRef = db.collection("servertime").document("current");

        serverTimeRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot document = task.getResult();

//                if(document.exists())
//                {
//                    Date serverDate = document.getDate("servertime", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
//                    callback.doneFetchingServerTime(serverDate);
//                }
//                else
//                {
                Map<String, Object> data = new HashMap<>();
                data.put("servertime", FieldValue.serverTimestamp());

                db.collection("servertime").document("current")
                .set(data)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
//                                checkResetTodaysReportCount();
//                        onPreExecute();
                        Date serverDate = document.getDate("servertime", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
                        callback.doneFetchingServerTime(serverDate);

                    }
                });
//                }
            }
        });
    }

    @Override
    protected Date doInBackground(String... strings) {
        if(android.os.Debug.isDebuggerConnected())
            android.os.Debug.waitForDebugger();
        Log.d("myLogTag", "doInBackground");
        return null;
    }

    @Override
    protected void onPostExecute(Date result) {

    }
}
