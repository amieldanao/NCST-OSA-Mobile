package com.telbound.utils;

public class UtilConstants {

    private static final String URL_BASE = "http://beta1.ncst-s.com/";
    public static final String URL_MEDIA = URL_BASE + "api/media/";
    public static final String URL_LOGIN = URL_BASE + "api/auth/login";
    public static final String URL_RECOVERY = URL_BASE + "api/auth/forgot_password";
    public static final String URL_ANNOUNCEMENTS = URL_BASE + "api/announcements";
    public static final String URL_ANNOUNCEMENTS_READ = URL_BASE + "api/announcements/read";
    public static final String URL_REPORTS = URL_BASE + "api/reports";
    public static final String URL_REPORTS_CATEGORIES = URL_BASE + "api/reports/categories";
    public static final String URL_REPORTS_CREATE = URL_BASE + "api/reports/actions/create";
    public static final String URL_THREADS = URL_BASE + "api/reports/threads";
    public static final String URL_THREADS_REPLY = URL_BASE + "api/reports/threads/reply";
    public static final String URL_GUIDEBOOK = URL_BASE + "api/guide-book";
    public static final String URL_CHANGE_PASSWORD = URL_BASE + "api/settings/change_password";
    public static final String URL_PROFILE_IMAGE_GET = URL_BASE + "api/settings/profile/image/get";
    public static final String URL_PROFILE_IMAGE_UPDATE = URL_BASE + "api/settings/profile/image/update";

}
