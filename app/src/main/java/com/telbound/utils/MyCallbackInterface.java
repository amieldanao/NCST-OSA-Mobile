package com.telbound.utils;

import java.util.Date;

public interface MyCallbackInterface {
    void doneFetchingServerTime(Date serverDate);
}
