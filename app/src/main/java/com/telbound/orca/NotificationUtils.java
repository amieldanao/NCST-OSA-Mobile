package com.telbound.orca;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class NotificationUtils
{
    private static final String TAG = "myLogTag";
    public static final int NOTIFICATION_ID = 1;

    public static final String ACTION_1 = "action_1";

    public static void displayNotification(Context context, String contextText) {

        Log.d(TAG, "trying to create notification");
        Intent action1Intent = new Intent(context, NotificationActionService.class)
                .setAction(ACTION_1);

        PendingIntent action1PendingIntent = PendingIntent.getService(context, 0,
                action1Intent, PendingIntent.FLAG_UPDATE_CURRENT);

       /* NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Sample Notification")
                        .setContentText("Notification text goes here")
                        .addAction(new NotificationCompat.Action(R.drawable.ic_launcher,
                                "Action 1", action1PendingIntent));*/

        NotificationCompat.Action act = new NotificationCompat.Action(R.drawable.ic_notification,
                "View Bill", action1PendingIntent);


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, "0")
                        .setSmallIcon(R.drawable.ncst_logo_256px)
                        .setContentTitle("Water Billing System")
                        .setContentText(contextText)
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .addAction(act);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public static class NotificationActionService extends IntentService
    {
        public NotificationActionService() {
            super(NotificationActionService.class.getSimpleName());
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            String action = intent.getAction();
            Log.d("LOGTAG", "Received notification action: " + action);
            if (ACTION_1.equals(action)) {
                // TODO: handle action 1.
                // If you want to cancel the notification: NotificationManagerCompat.from(this).cancel(NOTIFICATION_ID);
                Log.d("LOGTAG", "HOORAY!");
                Intent myIntent = new Intent(MainActivity.myContext, MainActivity.class);
                MainActivity.instance.startActivity(myIntent);
//                MainActivity.instance.showBillPage();
                //MainActivity.SetCurrentPage(0);
                NotificationManagerCompat.from(MainActivity.myContext).cancel(NOTIFICATION_ID);
            }
        }
    }
}