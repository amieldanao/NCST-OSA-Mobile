package com.telbound.orca;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;
import com.telbound.utils.UtilConstants;
import com.telbound.utils.UtilSession;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = SplashActivity.class.getSimpleName();
    private UtilSession utilSession;
    private String uuid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        utilSession = new UtilSession(this);

        if (utilSession.isLoggedIn()) {
            HashMap<String, String> data = utilSession.getDetails();
            uuid = data.get(UtilSession.KEY_UUID);

            prepareData();
        } else {
            Intent i = new Intent(this, AuthActivity.class);
            startActivity(i);
            finish();
        }
    }

    private void prepareData() {
        String route = UtilConstants.URL_ANNOUNCEMENTS;

        JSONObject param = new JSONObject();
        try {
            param.put("uuid", uuid);
        } catch (JSONException ignored) {}

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                route, param,
                response -> {
                    try {
                        if (response.getBoolean("error")) {
                            Log.e(TAG, response.getString("message"));
                            utilSession.logout();
                        } else {
                            Intent i = new Intent(this, MainActivity.class);
                            startActivity(i);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                Throwable::printStackTrace
        );

        RetryPolicy retryPolicy = new DefaultRetryPolicy(
                0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        );

        request.setRetryPolicy(retryPolicy);
        request.setShouldCache(false);

        AppController.getInstance().addToRequestQueue(request, TAG);
    }
}
