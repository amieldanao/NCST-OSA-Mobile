package com.telbound.orca;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.util.LruCache;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.telbound.adapters.MyNotifModelRecyclerViewAdapter;
import com.telbound.fragments.main.AccountFragment;
import com.telbound.fragments.main.AnnouncementFragment;
import com.telbound.fragments.main.HelpFragment;
import com.telbound.fragments.main.NotifFragment;
import com.telbound.fragments.main.ReportsFragment;
import com.telbound.models.NotifModel;
import com.telbound.utils.UtilFragments;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "myLogTag";
    private static FragmentManager fragmentManager;
//    private ActionBar actionBar;

    public static Context myContext;
    public static MainActivity instance;

    private LruCache<String, Bitmap> memoryCache;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private FirebaseFirestore db;
    private ListenerRegistration notificationListener;
    private ListenerRegistration notificationListenerImportant;
    private HashMap<String, ListenerRegistration> messagesListeners;

    public List<NotifModel> notifList = new ArrayList<>();

    public MyNotifModelRecyclerViewAdapter myAdapter;
    public Toolbar myToolbar;
    public Button notifButton;
    public TextView notifCounter;

    public int totalNotifs = 0;

    public SwipeRefreshLayout mSwipeRefreshLayout;

    public Set<String> viewedNotifications;
    public Set<String> viewedNewMessages;
    private BottomNavigationBar bottomNavigationBar;

    public HashMap<String, String> cached_student_names = new HashMap<>();
//    public final String TIME_SERVER = "time-a.nist.gov";
    public Date serverDate;
//    private LocationManager locationManager;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        fragmentManager = getSupportFragmentManager();

        db = FirebaseFirestore.getInstance();

        initBitmapCache();

        myContext = getApplicationContext();

        instance = this;

        bottomNavigationBar = findViewById(R.id.bottom_bar);
        initNavigation();

        Bundle extras = getIntent().getExtras();

        messagesListeners = new HashMap<>();

        Dexter.withActivity(this).withPermissions(
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).withListener(
                new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (extras == null) {
                                //Cry about not being clicked on
                                Log.d(TAG, "NULL CLICK!");
                                replaceHomeFragment();

                                bottomNavigationBar.selectTab(0, false);
                            } else if (extras.get("EXTRA_GOTO_FRAGMENT") != null) {
                                if (UtilFragments.ReportsFragment.equals(extras.get("EXTRA_GOTO_FRAGMENT"))) {
                                    bottomNavigationBar.selectTabAndTriggerListener(1, false);//
                                }
                            } else if (extras.getBoolean("NotifClicked")) {
                                fragmentManager
                                        .beginTransaction()
                                        .replace(R.id.frameContainer, new NotifFragment(),
                                                UtilFragments.NotifFragment).commit();
                            }

//                            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//
//                                return;
//                            }

//                            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//                            if(location != null) {
//                                long time = location.getTime();
//
//                                if (time > 0)
//                                    serverDate = new Date(time);
//                            }
//                            else
//                            {
//                                Log.d(TAG,"location is null! ");
//                            }

                            Map<String, Object> map = new HashMap<>();
                            map.put("servertime", FieldValue.serverTimestamp());
                            FirebaseFirestore.getInstance().collection("servertime").document("current")
                                    .set(map, SetOptions.merge())
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if(task.isSuccessful()) {
                                                FirebaseFirestore.getInstance().collection("servertime").document("current")
                                                        .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task2) {

                                                        if(task2.isSuccessful()) {
                                                            Timestamp timeStamp = (Timestamp) task2.getResult().get("servertime", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
                                                            serverDate = timeStamp.toDate();
                                                        }
                                                        else
                                                            serverDate = new Date();

                                                        Log.d(TAG, "ServerDate = " + serverDate.toString());

                                                        attachNotificationCallback();
                                                        attachMessagesCallback();
                                                        initFloatingActionButton();
                                                    }
                                                });
                                            }
                                            else
                                            {
                                                serverDate = new Date();

                                                attachNotificationCallback();
                                                attachMessagesCallback();
                                                initFloatingActionButton();
                                            }
                                        }
                                    });

                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }
        ).withErrorListener(
                error -> Toast.makeText(getApplicationContext(),
                        String.format("Error: %s", error.toString()),
                        Toast.LENGTH_LONG).show()
        ).onSameThread().check();


        if (mAuthListener != null)
            FirebaseAuth.getInstance().removeAuthStateListener(mAuthListener);

        mAuthListener = firebaseAuth -> {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            Log.d(TAG, "user logged out");
            if (user == null) {
                FragmentManager fm = getSupportFragmentManager();
                for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
                // Sign out logic here.
                Intent i = new Intent(myContext, AuthActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(i);
                finish();
            }
        };

        FirebaseAuth.getInstance().addAuthStateListener(mAuthListener);
    }


    public void savePreference() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.putStringSet("viewedNotifications", viewedNotifications);
        editor.apply();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        myContext = getApplicationContext();

        instance = this;
        BottomNavigationBar nav = findViewById(R.id.bottom_bar);
        Bundle extras = intent.getExtras();
        if (extras == null) {
            //Cry about not being clicked on
            Log.d(TAG, "NULL CLICK!");
            attachNotificationCallback();
            replaceHomeFragment();

            nav.selectTab(0, false);
        } else if (extras.getBoolean("NotifClicked")) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.frameContainer, new NotifFragment(),
                            UtilFragments.NotifFragment).commit();
        }

//        attachMessagesCallback();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "MainActivity destroyed!");
    }

    public void clearBitmaps() {
        memoryCache.evictAll();
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        memoryCache.put(key, bitmap);
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return memoryCache.get(key);
    }


    public void initBitmapCache() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        memoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public void clearNotifications() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

//    public void startFetchServerTime(MyCallbackInterface callback) {
//        new FetchServerTimeTask(callback).execute();
//    }


    public void attachMessagesCallback() {
//        startFetchServerTime(new MyCallbackInterface() {
//            @Override
//            public void doneFetchingServerTime(Date startFetchDate1) {
//        long currentDateMillis = getCurrentNetworkTime();
//        if(currentDateMillis > 0)
//            serverDate = new Date(currentDateMillis);
//
//        if(serverDate == null)
//            serverDate = new Date();



        if (AuthActivity.instance != null && AuthActivity.instance.loggedStudent != null) {

            db.collection("reports").whereGreaterThan("status", 0).whereArrayContains("senders", AuthActivity.instance.loggedStudent.getEmail())
                    .get().addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
    //                                SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
    //
    //                                viewedNewMessages = new HashSet<>(sharedPref.getStringSet("viewedNewMessages", new HashSet<String>()));

                            QuerySnapshot querySnapshot = task.getResult();

                            for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                                String thisReportKey = documentSnapshot.getId();
                                Query query = db.collection("reports").document(thisReportKey).collection("messages")
                                        .whereGreaterThan("date", serverDate);

                                if (messagesListeners.containsKey(thisReportKey))
                                    messagesListeners.get(thisReportKey).remove();

                                messagesListeners.put(thisReportKey, query.addSnapshotListener(((queryDocumentSnapshots, e) -> {
                                    if (e != null) {
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        if (user != null) {

                                            Log.w(TAG, "listen:error", e);
                                        }
                                        return;
                                    }


                                    final int[] i = {1};
                                    for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                                        if (dc.getType() == DocumentChange.Type.ADDED) {
    //                                                startFetchServerTime(new MyCallbackInterface() {
    //                                                    @Override
    //                                                    public void doneFetchingServerTime(Date startFetchDate) {

    //                                                if(!viewedNewMessages.contains(dc.getDocument().getId())) {
                                                    Date thisMessageDate = dc.getDocument().getDate("date", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
                                                    String sender = dc.getDocument().get("user", String.class);

                                                    boolean messageIsNew = thisMessageDate.after(serverDate);
                                                    if (messageIsNew && !AuthActivity.instance.loggedStudent.getEmail().equals(sender)) {
                                                        showNewMessageNotification(
                                                                "New message from " + dc.getDocument().get("reportId", String.class),
                                                                dc.getDocument().get("message", String.class),
                                                                dc.getDocument().get("key", String.class),
                                                                dc.getDocument().get("reportId", String.class),
                                                                i[0]);
                                                    }
                                                    i[0]++;

    //                                                    }
    //                                                });

                                        }
                                    }
                                })));
                            }
                        }
                    });
        } else {
            FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
            // Sign out logic here.
            Intent i = new Intent(myContext, AuthActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(i);
        }


//            }
//        });

    }

    public void showNewMessageNotification(String sender, String message, String reportKey, String reportId, int id) {
        if(reportKey == null)
            return;

        Log.d(TAG, "trying to create notification \n " + message);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);



//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            if (notificationManager.getNotificationChannel(reportKey) != null)
//                return;
//        }
        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplicationContext().getPackageName() + "/" + R.raw.notif);

        //int id = -1 * totalNotifs;
        int requestID = (int) System.currentTimeMillis();

        Intent resultIntent = new Intent(this, Chat.class);
        resultIntent.putExtra("EXTRA_REPORT_ID", reportId);
        resultIntent.putExtra("EXTRA_REPORT_KEY", reportKey);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.putExtra("NewMessageClicked", true);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        requestID,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(reportKey, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT);

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            notificationChannel.setSound(sound, audioAttributes);

            notificationManager.createNotificationChannel(notificationChannel);
        }
        else
        {
            MediaPlayer mp= MediaPlayer.create(getApplicationContext(), R.raw.notif);
            mp.start();
        }


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, reportKey);


        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ncst_logo_256px)
                .setTicker("Hearty365")
                //     .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle(sender)
                .setContentText(Html.fromHtml(message))
                .setContentIntent(resultPendingIntent)
                .setContentInfo("Info");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setSound(null);
        }


        notificationManager.notify(id, notificationBuilder.build());
    }

    public void attachNotificationCallback() {

        if(AuthActivity.instance != null && AuthActivity.instance.loggedStudent != null)
        {
            if(mSwipeRefreshLayout != null)
                mSwipeRefreshLayout.setRefreshing(true);
            notifList.clear();
            // We only listen to notifications that are not resolved yet


            if(notificationListener != null)
                notificationListener.remove();

            if(notificationListenerImportant != null)
                notificationListenerImportant.remove();

            // dont show notifications that are viewed already
            SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

            viewedNotifications = new HashSet<>(Objects.requireNonNull(sharedPref.getStringSet("viewedNotifications", new HashSet<>())));
            Log.d(TAG, "");
            EventListener<QuerySnapshot> eventListener = new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {

                    if (e != null) {
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        if (user != null) {

                            Log.w(TAG, "listen:error", e);
//                                showToast(e.getMessage());
                        }
                        return;
                    }

                    assert snapshots != null;
                    if(snapshots.getDocuments().size() == 0)
                    {
//                            showToast("No reports.");
                        if(myAdapter != null)
                            myAdapter.notifyDataSetChanged();
                        if(mSwipeRefreshLayout != null)
                            mSwipeRefreshLayout.setRefreshing(false);
                        return;
                    }

                    totalNotifs = 0;

                    int i = 0;
                    for (DocumentChange dc : snapshots.getDocumentChanges()) {

                        NotifModel notifModel = dc.getDocument().toObject(NotifModel.class);
                        notifModel.key = dc.getDocument().getId();

                        switch (dc.getType())
                        {
                            case ADDED:
                                if(notifModel.getRecipients().contains(AuthActivity.instance.loggedStudent.getEmail()) || notifModel.getRecipients().contains(AuthActivity.instance.loggedStudent.getID())) {

                                    NotifModel existingNotif = hasNotifAlready(notifModel.key);
                                    if(existingNotif == null) {
                                        notifList.add(notifModel);
                                        totalNotifs ++;
                                    }


                                    if(notifModel.getType() != null && notifModel.getType().equals("invalidation")) {
                                        if(notifModel.getSeenedBy() != null) {
                                            if(!notifModel.getSeenedBy().contains(AuthActivity.instance.loggedStudent.getEmail())) {
                                                sendNotification("NCST-OSA Notification!", dc.getDocument().get("title").toString(), dc.getDocument().getId());
                                                setViewedNotification(dc.getDocument().getId(), notifModel.getSeenedBy());
                                            }
                                            else
                                            {
                                                totalNotifs--;
                                            }
                                        }

//                                        if(viewedNotifications.contains(notifModel.key))
//                                            viewedNotifications.add(notifModel.key);
                                    }
                                    else
                                    {
                                        if(!viewedNotifications.contains(notifModel.key)) {
                                            sendNotification("NCST-OSA Notification!", dc.getDocument().get("title").toString(), dc.getDocument().getId());
                                            viewedNotifications.add(notifModel.key);
                                            savePreference();
                                        }
                                        else
                                            totalNotifs--;
                                    }

                                    if(notifCounter != null)
                                        notifCounter.setText(String.valueOf(Math.max(0, totalNotifs)));
                                }
                            break;


                        }

//                        switch (dc.getType()) {
//
//                            case ADDED:
//                                NotifModel existingNotif = hasNotifAlready(dc.getDocument().getId());
//
//                                if (existingNotif == null) {
//
//                                    NotifModel newNotifModel = new NotifModel(dc.getDocument().get("message").toString(), dc.getDocument().get("title").toString(), dc.getDocument().getDate("dateSent"));
//                                    newNotifModel.key = dc.getDocument().getId();
//                                    notifList.add(newNotifModel);
//                                    if(myAdapter != null)
//                                        myAdapter.notifyDataSetChanged();
//                                }
//
//                                if(viewedNotifications.contains(dc.getDocument().getId()))
//                                {
//                                    totalNotifs = Math.max(0, totalNotifs-1);
//                                }
//                                else if(existingNotif == null){
//                                    sendNotification("NCST-OSA Notification!", dc.getDocument().get("title").toString(), dc.getDocument().getId());
//                                    totalNotifs ++;
//                                    viewedNotifications.add(dc.getDocument().getId());
//
//                                    setViewedNotification(dc.getDocument().getId(), thisNotifRecipients);
//                                }
//
//                                savePreference();
//
//                                break;
//                            case MODIFIED:
//
//                                if(thisNotifRecipients.contains(AuthActivity.instance.loggedStudent.getEmail()))
//                                {
//                                    setViewedNotification(dc.getDocument().getId(), thisNotifRecipients);
//                                }
//
//                                if(dc.getDocument().get("title", String.class).contains("re-opened")) {
//
//                                    NotifModel existingNotifModified = hasNotifAlready(dc.getDocument().getId());
//
//                                    if (existingNotifModified != null) {
//                                        notifList.remove(existingNotifModified);
//                                    }
//
//                                    NotifModel newNotifModel = new NotifModel(dc.getDocument().get("message").toString(), dc.getDocument().get("title").toString(), dc.getDocument().getDate("dateSent"));
//                                    newNotifModel.key = dc.getDocument().getId();
//                                    notifList.add(newNotifModel);
//
//                                    if (myAdapter != null)
//                                        myAdapter.notifyDataSetChanged();
//
//                                    if (viewedNotifications.contains(dc.getDocument().getId())) {
//                                        viewedNotifications.remove(dc.getDocument().getId());
//
//                                    }
//                                    else
//                                    {
//                                        viewedNotifications.add(dc.getDocument().getId());
//                                    }
//
//
//                                    savePreference();
//
//                                    sendNotification("NCST-OSA Notification!", dc.getDocument().get("title").toString(), dc.getDocument().getId() + "re-open");
//
//
//
//                                    totalNotifs++;
//                                }
//                                else
//                                {
//                                    if(thisNotifRecipients.contains(AuthActivity.instance.loggedStudent.getEmail()))
//                                    {
//                                        sendNotification("NCST-OSA Notification!", dc.getDocument().get("title").toString(), dc.getDocument().getId() + "re-open");
//                                    }
//                                }
//
//                                break;
//                            case REMOVED:
//                                if (viewedNotifications.contains(dc.getDocument().getId())) {
//                                    viewedNotifications.remove(dc.getDocument().getId());
//
//                                }
//
//                                break;
//                        }



//                        i++;
//
//                        notifCounter.setText(String.valueOf(totalNotifs));
//                        if(i >= snapshots.getDocumentChanges().size())
//                        {
                            if(myAdapter != null)
                                myAdapter.notifyDataSetChanged();
                            if(mSwipeRefreshLayout != null)
                                mSwipeRefreshLayout.setRefreshing(false);
//                        }
                    }
                }
            };

            Query query = db.collection("notifications").whereArrayContains("recipients", AuthActivity.instance.loggedStudent.getID());
            notificationListener = query.addSnapshotListener(eventListener);

            Query query2 = db.collection("notifications").whereArrayContains("recipients", AuthActivity.instance.loggedStudent.getEmail());
            notificationListenerImportant = query2.addSnapshotListener(eventListener);


        }
        else
        {
            FragmentManager fm = getSupportFragmentManager();
            for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
            // Sign out logic here.
            Intent i = new Intent(myContext, AuthActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(i);
        }
    }

    private void setViewedNotification(String id, List<String> origSeenRecipients)
    {
        String myEmail = AuthActivity.instance.loggedStudent.getEmail();
        if(origSeenRecipients == null)
            origSeenRecipients = new ArrayList<>();

        if(!origSeenRecipients.contains(myEmail))
            origSeenRecipients.add(myEmail);

        Map<String, Object> map = new HashMap<>();
        map.put("seenedBy", origSeenRecipients);

        FirebaseFirestore.getInstance().collection("notifications").document(id)
                .set(map, SetOptions.merge())
        .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.d(TAG, "I viewed this notification : " + id + ", " + AuthActivity.instance.loggedStudent.getEmail());
            }
        });
    }

    public NotifModel hasNotifAlready(String key) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return notifList.stream().filter(notifModel -> key.equals(notifModel.key)).findFirst().orElse(null);
        }
        else
        {
            return hasNotifAlreadyOLD(key);
        }
    }

    private NotifModel hasNotifAlreadyOLD(String key)
    {
        for (NotifModel n : notifList) {
            if(n.key.equals(key))
                return n;
        }

        return null;
    }

    public void sendNotification(String dateSent, String message, String notifChannel) {

        Log.d(TAG, "trying to create notification \n " + message);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            if (notificationManager.getNotificationChannel(notifChannel) != null)
//                return;
        }


        int id = -1 * totalNotifs;
        int requestID = (int) System.currentTimeMillis();

        Intent resultIntent = new Intent(this, MainActivity.class);
//        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        resultIntent.putExtra("NotifClicked", true);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        requestID,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(notifChannel, "My Notifications", NotificationManager.IMPORTANCE_MAX);

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, notifChannel);


        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ncst_logo_256px)
                .setTicker("Hearty365")
                //     .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle(dateSent)
                .setContentText(Html.fromHtml(message))
                .setContentIntent(resultPendingIntent)
                .setContentInfo("Info");


        notificationManager.notify(id, notificationBuilder.build());
    }




    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.perm_title);
        builder.setMessage(R.string.perm_message);
        builder.setPositiveButton(getString(R.string.perm_setings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(R.string.perm_cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void initFloatingActionButton() {
        FloatingActionButton fab = findViewById(R.id.fab_options);
        fab.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), ComposerActivity.class);
            startActivityForResult(i, 1);
        });
    }

    private void selectNavigationTab(int position){
        switch (position) {
            case 0:
                replaceHomeFragment();
                break;
            case 1:
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameContainer, new ReportsFragment(),
                                UtilFragments.ReportsFragment).commit();
                break;
            case 2:
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameContainer, new HelpFragment(),
                                UtilFragments.HelpFragment).commit();
                break;
            case 3:
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.frameContainer, new AccountFragment(),
                                UtilFragments.AccountFragment).commit();
                break;
        }
    }

    private void initNavigation() {
//        nav.selectTab(0, false);
        bottomNavigationBar.addTab(new BottomBarItem(R.drawable.ic_home, R.string.nav_home));
        bottomNavigationBar.addTab(new BottomBarItem(R.drawable.ic_message, R.string.nav_message));
        bottomNavigationBar.addTab(new BottomBarItem(R.drawable.ic_explore, R.string.nav_explore));
        bottomNavigationBar.addTab(new BottomBarItem(R.drawable.ic_account, R.string.nav_account));

        bottomNavigationBar.setOnReselectListener(this::selectNavigationTab);

        bottomNavigationBar.setOnSelectListener(this::selectNavigationTab);
    }

//    private void setABar() {
//        if (! Objects.requireNonNull(getSupportActionBar()).isShowing()) {
//            getSupportActionBar().show();
//        }
//    }

    public void replaceHomeFragment() {
        fragmentManager
            .beginTransaction()
            .replace(R.id.frameContainer, new AnnouncementFragment(),
                UtilFragments.HomeFragment).commit();
    }

    public void setTitle(String title) {

//        actionBar.setTitle(title);
        myToolbar.setTitle(title);
    }

    public void initToolbar(View root)
    {
        if(root == null)
            return;
        myToolbar = root.findViewById(R.id.main_toolbar);
        notifButton = root.findViewById(R.id.btn_notif_button);
        notifCounter = root.findViewById(R.id.txt_notif_counter);

        notifCounter.setText(String.valueOf(totalNotifs));

        notifButton.setOnClickListener(view -> {
//                showToast("Clicked notifButton");
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.frameContainer, new NotifFragment(),
                            UtilFragments.NotifFragment).commit();
        });

        setSupportActionBar(myToolbar);
    }

    // create an action bar button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        return true;

//        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {


        return true;
    }

    public void preventClicks(View view) {}
}
