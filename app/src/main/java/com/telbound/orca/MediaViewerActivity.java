package com.telbound.orca;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.asura.library.posters.Poster;
import com.asura.library.posters.RemoteImage;
import com.asura.library.posters.RemoteVideo;
import com.asura.library.views.PosterSlider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MediaViewerActivity extends AppCompatActivity {
    private static final String TAG = MediaViewerActivity.class.getSimpleName();

    private JSONObject jsonObject;
    private JSONArray jsonArray;

    PosterSlider posterSlider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_viewer);

//        ActionBar actionBar = getSupportActionBar();
//        assert actionBar != null;
//        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1690FA")));
//        actionBar.setDisplayHomeAsUpEnabled(true);

        jsonObject = new JSONObject();
        jsonArray = new JSONArray();
        posterSlider = findViewById(R.id.poster_slider);

        Intent i = getIntent();

        try {
            jsonArray = new JSONArray(
                    Objects.requireNonNull(i.getStringExtra("EXTRA_MEDIA_ARRAY"))
            );

            loadMediaArray();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadMediaArray() {
        List<Poster> posters = new ArrayList<>();

        try {
            for (int i=0; i<jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);

                if (isImageFile(jsonObject.getString("file"))) {
                    posters.add(new RemoteImage(jsonObject.getString("file")));
                } else {
                    if (isVideoFile(jsonObject.getString("file"))) {
                        posters.add(new RemoteVideo(Uri.parse(jsonObject.getString("file"))));
                    }
                }
            }

            posterSlider.setPosters(posters);
        } catch (JSONException e) {
            e.printStackTrace();

            Log.e(TAG, Objects.requireNonNull(e.getMessage()));
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private static boolean isImageFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("image");
    }

    private static boolean isVideoFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("video");
    }

//    private void loadImageSlider() {
//        List<SlideModel> slideModels = new ArrayList<>();
//
//        for (int i=0; i<jsonArray.length(); i++) {
//            try {
//                jsonObject = jsonArray.getJSONObject(i);
//                slideModels.add(new SlideModel(jsonObject.getString("file"), "", true));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//
//        ImageSlider imageSlider = findViewById(R.id.media_image_slider);
//        imageSlider.setImageList(slideModels, true);
//        imageSlider.startSliding(3000);
//    }
//
//    private void loadVideoPlayer() {
//        try {
//            jsonObject = jsonArray.getJSONObject(0);
//
//            AndExoPlayerView andExoPlayerView = findViewById(R.id.media_video_player);
//            andExoPlayerView.setPlayWhenReady(true);
//            andExoPlayerView.setSource(jsonObject.getString("file"));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
