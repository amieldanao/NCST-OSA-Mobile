package com.telbound.orca;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.telbound.models.Message;
import com.telbound.models.Upload;
import com.telbound.utils.MyUtilMedia;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyUploadService extends Service {
    private static final String TAG = "myLogTag";
    private static final String CHANNEL_ID_DEFAULT = "default";

    public String report_id, report_key;
    //    private DatabaseReference myReference;

    static final int PROGRESS_NOTIFICATION_ID = 0;

    //    public MyUploadService(String name) {
    //        super(name);
    //    }
    //
    //    public MyUploadService()
    //    {
    //        super("MyUploadService");
    //    }



    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {


        super.onStartCommand(intent, flags, startId);

        String[] array_uri = intent.getStringArrayExtra("array_uri");
        String[] array_filename = intent.getStringArrayExtra("array_filename");
        String[] array_originalFileName = intent.getStringArrayExtra("array_originalFileName");
        long[] array_fileSize = intent.getLongArrayExtra("array_fileSize");
        report_id = intent.getStringExtra("report_id");
        report_key = intent.getStringExtra("report_key");
        //        myReference = FirebaseDatabase.getInstance().getReference().child("reports/" + report_id + "/thread");


        for (int i = 0; i < array_uri.length; i++) {
            if(!Chat.instance.skippedFiles.contains(array_originalFileName[i]) && array_uri[i] != null)
                continueUpload(array_uri[i], array_filename[i], array_originalFileName[i], array_fileSize[i]);
        }

        return START_REDELIVER_INTENT;
        //        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //    @Override
    //    protected void onHandleIntent(@Nullable Intent intent) {
    //
    //    }
    private void createDefaultChannel() {
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID_DEFAULT,
                    "Default",
                    NotificationManager.IMPORTANCE_DEFAULT);
            nm.createNotificationChannel(channel);
        }
    }

    protected void showProgressNotification(String caption, long completedUnits, long totalUnits) {
        int percentComplete = 0;
        if (totalUnits > 0) {
            percentComplete = (int)(100 * completedUnits / totalUnits);
        }

        createDefaultChannel();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID_DEFAULT)
                .setSmallIcon(R.drawable.ncst_logo_90px)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(caption)
                .setProgress(100, percentComplete, false)
                .setOngoing(true)
                .setAutoCancel(true);

        NotificationManager manager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        manager.notify(PROGRESS_NOTIFICATION_ID, builder.build());
    }

    private void continueUpload(String uri, String filenameOnly, String origFilenameOnly, long fileSize) {
        try {

            byte[] byteArray = MyUtilMedia.getBytes(this, Uri.fromFile(new File(uri)));
            byte[] finalByteArray = byteArray;

            Log.d(TAG, "start uploading " + filenameOnly);

            String mimeType = URLConnection.guessContentTypeFromName(uri);

            //we only compress still images not gif
            if (MyUtilMedia.isImageFile(uri) && !mimeType.equals("image/gif")) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inMutable = true;
                Bitmap theBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);


                if (theBitmap != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    theBitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                    finalByteArray = baos.toByteArray();
                }
            }



            StorageReference reference = FirebaseStorage.getInstance().getReference()
                    .child("reports/" + report_id + "/" + filenameOnly);

            UploadTask thisUploadTask = (UploadTask) reference.putBytes(finalByteArray);

            thisUploadTask
                    .addOnProgressListener(new OnProgressListener < UploadTask.TaskSnapshot > () {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            //                    showProgressNotification("NCST-OSA is running on background", taskSnapshot.getBytesTransferred(), taskSnapshot.getTotalByteCount());
                            //                    Chat.instance.updateProgressUpload(filenameOnly, taskSnapshot);

                            Chat.instance.updateProgressUpload(filenameOnly, taskSnapshot);

                            String totalProgressText = MyUtilMedia.getProperFileSize(taskSnapshot.getBytesTransferred()) + "/" + MyUtilMedia.getProperFileSize(taskSnapshot.getTotalByteCount());
                            Log.d(TAG, "uploading " + filenameOnly + " : " + totalProgressText);
                        }
                    }).

                    continueWithTask(new Continuation < UploadTask.TaskSnapshot, Task < Uri >> () {
                        @Override
                        public Task < Uri > then(@NonNull Task < UploadTask.TaskSnapshot > task) throws Exception {
                            // Forward any exceptions
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }

                            Log.d(TAG, "uploadFromUri: upload success");

                            // Request the public download URL
                            return reference.getDownloadUrl();
                        }
                    }).addOnSuccessListener(new OnSuccessListener < Uri > () {
                @Override
                public void onSuccess(Uri downloadUri) {



                    //Chat.instance.updateProgressUpload(filenameOnly, taskSnapshot);

                    Log.d(TAG, "Done upload file " + uri);

                    if (mimeType.contains("video")) {
                        Chat.instance.uploadVideoThumbnail(uri, filenameOnly);
                    } else {
                        Chat.instance.updateMetaData(filenameOnly, mimeType);
                    }

                    Chat.instance.uploadingTasks.remove(filenameOnly);


                    //upload message on database

                    DateFormat sdf = new SimpleDateFormat("YYYYMMdd MMMM dd hh:mm:ss a");

                    Date date = new Date();
//                    String current_time = sdf.format(date);

                    //                    Message newMessage = new Message(downloadUri.toString(), current_time, Chat.instance.UserEmail, Chat.instance.UserID, true, filenameOnly, false);
                    Message newMessage = new Message(downloadUri.toString(), Chat.instance.UserEmail, Chat.instance.UserID, true, filenameOnly, false);
                    newMessage.setKey(Chat.instance.thisReportData.getKey());
                    newMessage.setReportId(Chat.instance.thisReportData.getReportId());


                    Log.d(TAG, "pushing to database : " + filenameOnly);

                    Chat.instance.db.collection("reports").document(report_key).collection("messages").document().set(newMessage)
                            .addOnSuccessListener(new OnSuccessListener < Void > () {
                                @Override
                                public void onSuccess(Void aVoid) {

                                }
                            });
                    //                    myReference.push().setValue(newMessage);


                    if (Chat.instance.uploadingTasks.isEmpty()) {
                        Log.d(TAG, "All uploaded from service!");
                    }


                    Chat.instance.checkIfAllUploaded();
                }
            })
            .addOnFailureListener(e -> Log.e(TAG, "Upload failed " + uri, e.getCause()));

            Upload newUpload = new Upload(filenameOnly, origFilenameOnly, thisUploadTask, fileSize);
            //            Chat.instance.uploadList.add(newUpload);
            //Chat.instance.notifyUploadDataState();

            Chat.instance.uploadingTasks.put(filenameOnly, thisUploadTask);

            //            Chat.instance.uploadingTasks.get(filenameOnly).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            //                @Override
            //                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
            //
            //
            //                    Chat.instance.updateProgressUpload(filenameOnly, taskSnapshot);
            //
            //                }
            //            });

            Chat.instance.uploadingTasks.get(filenameOnly).addOnCanceledListener(new OnCanceledListener() {
                @Override
                public void onCanceled() {
                    //                    ProgressBar progressBar = Chat.instance.getProgressBarViewByFileName(filenameOnly);
                    //                    if(progressBar != null)
                    //                    {
                    //                        progressBar.setMax(1);
                    //                        progressBar.setProgress(1);
                    //
                    //                        Drawable progressDrawable = progressBar.getProgressDrawable().mutate();
                    //                        progressDrawable.setColorFilter(Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);
                    //                        progressBar.setProgressDrawable(progressDrawable);
                    //                    }

                    Chat.instance.uploadingTasks.remove(filenameOnly);
                    Chat.instance.skippedFiles.add(filenameOnly);
                    Chat.instance.checkIfAllUploaded();
                }
            });


        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }
    }




}