package com.telbound.orca;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.telbound.fragments.auth.LoginFragment;
import com.telbound.models.Student;
import com.telbound.utils.UtilFragments;

import java.util.List;

public class AuthActivity extends AppCompatActivity {
    private static FragmentManager fragmentManager;
    public static AuthActivity instance;
    public Student loggedStudent;
    public Bitmap profilePic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        fragmentManager = getSupportFragmentManager();

        instance = this;

        Dexter.withActivity(this).withPermissions(
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).withListener(
            new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport report) {
                    if (report.areAllPermissionsGranted()) {
                        // Replace main layout to show login
                        replaceLoginFragment();
                    }

                    if (report.isAnyPermissionPermanentlyDenied()) {
                        // Navigate to settings
                        showSettingsDialog();
                    }
                }

                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                    token.continuePermissionRequest();
                }
            }
        ).withErrorListener(error -> Toast.makeText(getApplicationContext(), String.format("Error: %s", error.toString()), Toast.LENGTH_LONG).show()).onSameThread().check();

        findViewById(R.id.auth_btn_close).setOnClickListener(v -> {
//            finish();
            System.exit(0);
        });

        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.perm_title);
        builder.setMessage(R.string.perm_message);
        builder.setPositiveButton(getString(R.string.perm_setings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(R.string.perm_cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    public void replaceLoginFragment() {
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_exit)
                .replace(R.id.frameContainer, new LoginFragment(),
                        UtilFragments.LoginFragment).commit();
    }

    @Override
    public void onBackPressed() {
////        Fragment RecoveryFragment = fragmentManager
////                .findFragmentByTag(UtilFragments.RecoveryFragment);
////
////        if (RecoveryFragment != null)
////            replaceLoginFragment();
////        else
//        replaceLoginFragment();


    }

    public void preventClicks(View view) {}
}
