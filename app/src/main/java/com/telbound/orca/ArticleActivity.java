package com.telbound.orca;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;
import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.telbound.utils.UtilConstants;
import com.telbound.utils.UtilSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ArticleActivity extends AppCompatActivity {
    private static final String TAG = ArticleActivity.class.getSimpleName();

    private  TextView a_title, a_content, a_posted;
    private  String uuid, route, aId;
    private ImageSlider imageSlider;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        Intent i = getIntent();
        aId = i.getStringExtra("a_id");

        UtilSession utilSession = new UtilSession(this);
        HashMap<String, String> details = utilSession.getDetails();

        uuid = details.get(UtilSession.KEY_UUID);
        route = UtilConstants.URL_ANNOUNCEMENTS_READ;

        actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1690FA")));
        actionBar.setDisplayHomeAsUpEnabled(true);

        a_title = findViewById(R.id.article_title);
        a_content = findViewById(R.id.article_content);
        a_posted = findViewById(R.id.article_posted);

        imageSlider = findViewById(R.id.carousel_view);

        progressBar();
        fetchArticle();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void fetchArticle() {

        JSONObject param = new JSONObject();
        try {
            param.put("announcement_id", aId);
            param.put("uuid", uuid);
        } catch (JSONException ignored) {}

        JsonObjectRequest request = new JsonObjectRequest(
            Request.Method.POST,
            route, param,
            response -> {
                try {
                    if (response.getBoolean("error")) {
                        progressBar();
                        Toast.makeText(this, response.getString("message"),
                                Toast.LENGTH_LONG).show();
                    } else {
                        JSONObject jObj = response.getJSONObject("data");
                        a_title.setText(jObj.getString("title"));
                        a_content.setText(Html.fromHtml(jObj.getString("content")));
                        a_posted.setText(jObj.getString("postedAt"));
                        actionBar.setTitle(jObj.getString("title"));

                        JSONArray a = jObj.getJSONArray("media");
                        ArrayList<SlideModel> images = new ArrayList<>();

                        for (int i=0; i<a.length(); i++) {
                            images.add(
                                new SlideModel(a.get(i).toString(), null, true)
                            );
                        }

                        imageSlider.setImageList(images, true);
                        imageSlider.startSliding(3000);

                        progressBar();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    progressBar();
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            },
            error -> {
                error.printStackTrace();

                progressBar();
                Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        );

        RetryPolicy retryPolicy = new DefaultRetryPolicy(
                0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        );

        request.setRetryPolicy(retryPolicy);
        request.setShouldCache(false);

        AppController.getInstance().addToRequestQueue(request, TAG);
    }

    private void progressBar() {
        View support_layout = findViewById(R.id.support_layout);

        if (support_layout.isShown()) {
            support_layout.setVisibility(View.GONE);
        } else {
            support_layout.setVisibility(View.VISIBLE);
        }
    }
}
