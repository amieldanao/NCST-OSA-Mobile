package com.telbound.orca;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.Formatter;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.fxn.pix.Options;
import com.fxn.utility.PermUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
//import com.google.firebase.database.ChildEventListener;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.Query;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.telbound.adapters.MessageAdapater;
import com.telbound.fragments.main.MediaView;
import com.telbound.fragments.main.NotifFragment;
import com.telbound.fragments.main.ReportsFragment;
import com.telbound.models.Category;
import com.telbound.models.Message;
import com.telbound.models.Report;
import com.telbound.models.Student;
import com.telbound.utils.MyUtilMedia;
import com.telbound.utils.PixCustom;
import com.telbound.utils.UtilFragments;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;


public class Chat extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, MediaView.OnFragmentInteractionListener {
    private static final String TAG = "myLogTag";
    private final int pixImageRequest = 666;

    private final int message_count_page = 15;

    public static Context context;
    public static Chat instance;

    private List<String> messagesTime;
    private int currentMessageTimeCheck;

//    private LinearLayout layout;
//    private RelativeLayout layout_2;
    private ImageView sendButton;
    private EditText messageArea;
//    private ScrollView scrollView;
    private ImageView mediaPickButton;
    private View my_chatbox_layout;
    private View my_uploads_layout;
    private TextView media_files_counter;
    private TextView txt_current_upload_name;
    private ProgressBar progressBar;


//    public DatabaseReference reference1;

    public String uuid, report_id, content;
    public String report_key;

    public String UserName, UserID, UserEmail;

    private ArrayList<String> selectedMedias;
    public ArrayList<String> skippedFiles;
    public HashMap<String, UploadTask> uploadingTasks;



    private RecyclerView recyclerView;
    public static MessageAdapater messageAdapater;
    public List<Message> messageList;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    public FirebaseFirestore db;
    private ListenerRegistration chatListener;
    private ListenerRegistration oldChatListener;

    private ListenerRegistration reportListener;

    private DocumentSnapshot oldestVisibleMessage;

    private List<String> addedMessages = new ArrayList<>();
//    private HashMap<String, Uri> cached_profile_image_urls = new HashMap<>();

    private ImageView report_info_button;

    public Report thisReportData;
    private Uri suspectPhotoUrl;
    private Toolbar myToolbar;
//    public HashMap<String, View> allUploadViews;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        db = FirebaseFirestore.getInstance();

        instance = this;
        context = this;

        Intent i = getIntent();
        if(i.hasExtra("EXTRA_REPORT_ID"))
            report_id = i.getStringExtra("EXTRA_REPORT_ID");
        if(i.hasExtra("EXTRA_REPORT_KEY"))
            report_key = i.getStringExtra("EXTRA_REPORT_KEY");

        initViews();
    }




    private void initViews()
    {
        sendButton = findViewById(R.id.sendButton);
        messageArea = findViewById(R.id.messageArea);
        mediaPickButton = findViewById(R.id.btn_chat_media);

        my_chatbox_layout = findViewById(R.id.my_chatbox_layout);
        my_uploads_layout = findViewById(R.id.my_uploads_layout);
        media_files_counter = findViewById(R.id.media_files_counter);
        txt_current_upload_name = findViewById(R.id.txt_current_upload_name);
        progressBar = findViewById(R.id.progressBar);



        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        myToolbar = findViewById(R.id.main_toolbar);

        if(report_id != null) {
            SpannableString s = new SpannableString(report_id);
            s.setSpan(new TypefaceSpan("open_sans_bold.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            setTitle(s);
        }

        setSupportActionBar(myToolbar);

        report_info_button = findViewById(R.id.report_info_button);

        // Show report info
        report_info_button.setOnClickListener(view -> {

            if(thisReportData != null)
            {
                LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View dialoglayout = inflater.inflate(R.layout.report_info_layout, null);

                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppTheme));
                builder.setTitle(thisReportData.getReportId());

                TextView report_date = dialoglayout.findViewById(R.id.txt_report_date);
                TextView report_categories = dialoglayout.findViewById(R.id.txt_report_categories);
                TextView suspect_name = dialoglayout.findViewById(R.id.txt_suspect_name);
                TextView suspect_gender = dialoglayout.findViewById(R.id.txt_suspect_gender);
                TextView suspect_descriptions = dialoglayout.findViewById(R.id.txt_suspect_descriptions);
                ImageView suspect_photo = dialoglayout.findViewById(R.id.suspect_photo);

                report_date.setText("Report Date : " + thisReportData.getPostedAt());
                List<String> categories_names = new ArrayList<>();
                for(Category category : thisReportData.getCategories())
                {
                    categories_names.add(category.getName());
                }
                report_categories.setText("Report Categories : " + categories_names.toString().replaceAll("\\[", "").replaceAll("\\]",""));
                suspect_name.setText("Suspect Name : " + (thisReportData.getSuspect().length() == 0 ? "Unknown" : thisReportData.getSuspect() + " " + thisReportData.getSuspect_surname()));
                suspect_gender.setText("Suspect Gender : " + thisReportData.getGender());
                suspect_descriptions.setText("Suspect Descriptions : " + thisReportData.getDescriptions().toString().replaceAll("\\[", "").replaceAll("\\]",""));

                if(suspectPhotoUrl != null)
                {
                    Glide.with(getApplicationContext())
                            .load(suspectPhotoUrl)
                            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                            .into(suspect_photo);
                }

                builder.setView(dialoglayout);

                builder.setNegativeButton("Close", (dialogInterface, i1) -> dialogInterface.dismiss());

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        messagesTime = new ArrayList<>();
        skippedFiles = new ArrayList<>();
        uploadingTasks = new HashMap<>();

        currentMessageTimeCheck = 0;
        selectedMedias = new ArrayList<>();




        UserName = AuthActivity.instance.loggedStudent.getFirst_name() + ' ' + AuthActivity.instance.loggedStudent.getLast_name();
        UserID = AuthActivity.instance.loggedStudent.getID();
        UserEmail = AuthActivity.instance.loggedStudent.getEmail();




        messageList = new ArrayList<>();
        recyclerView = findViewById(R.id.chat_recycler_view);
        messageAdapater = new MessageAdapater(context,this);
        recyclerView.setAdapter(messageAdapater);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mediaPickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPixImagePicker();
            }
        });

//        reference1 = FirebaseDatabase.getInstance().getReference().child("reports/" + report_id + "/thread");

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        attachReportListener();



    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        report_id = intent.getStringExtra("EXTRA_REPORT_ID");
        report_key = intent.getStringExtra("EXTRA_REPORT_KEY");

        if(report_id == null) {
            Log.d(TAG, "NULL");
            initViews();
        }
        else
        {
            Log.d(TAG, report_id);
        }


    }

    private void attachReportListener()
    {
        // Try get reportData
        if(report_key != null) {

            if(thisReportData != null)
            {

                if(thisReportData.getStatus() <= 0)
                {
                    if(thisReportData.getStatus() == 0)
                        showToast("Report is now resolved!");

                    if(thisReportData.getStatus() == -1)
                        showToast("Report is invalid!");
                    onBackPressed();
                }

                if(reportListener != null)
                    reportListener.remove();

                Query query = db.collection("reports").whereEqualTo("reportId", thisReportData.getReportId());

                reportListener = query.addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) {
                            Log.w(TAG, "listen:error", e);
                            showToast(e.getMessage());
                        return;
                    }


                    if(queryDocumentSnapshots.size() > 0)
                    {
                        for(DocumentChange documentChange : queryDocumentSnapshots.getDocumentChanges())
                        {
                            switch (documentChange.getType()) {

                                case ADDED:
                                    if(thisReportData == null) {
                                        thisReportData = documentChange.getDocument().toObject(Report.class);
                                        thisReportData.setKey(documentChange.getDocument().getId());
                                        thisReportData.setReportId(documentChange.getDocument().get("reportId", String.class));
                                    }

                                    break;

                                case MODIFIED:
                                    int status = documentChange.getDocument().get("status", Integer.class);

                                    // check if we're still in this report
                                    List<String> current_senders = (List<String>) documentChange.getDocument().get("senders");

                                    if(!current_senders.contains(AuthActivity.instance.loggedStudent.getEmail()))
                                    {
                                        showToast("You have been moved to a separate report");
                                        onBackPressed();
                                        break;
                                    }

                                    if(status <= 0)
                                    {
                                        if(status == 0)
                                            showToast("Report is now resolved!");

                                        if(status == -1)
                                            showToast("Report is invalid!");

                                        onBackPressed();
                                    }
                                    else
                                    {
                                        thisReportData = documentChange.getDocument().toObject(Report.class);
                                        thisReportData.setKey(documentChange.getDocument().getId());
                                        thisReportData.setReportId(documentChange.getDocument().get("reportId", String.class));
                                    }

                                    break;

                                case REMOVED:
                                    showToast("Report no longer exists!");
                                    onBackPressed();
                                    break;
                            }
                        }

                        fetchMessages();
                    }
                    else
                    {
                        onBackPressed();
                    }
                });
            }
            else {
                db.collection("reports").document(report_key)
                        .get().addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        thisReportData = documentSnapshot.toObject(Report.class);
                        if (thisReportData.getSuspect().length() > 0) {
                            db.collection("students").whereEqualTo("first_name", thisReportData.getSuspect()).whereEqualTo("last_name", thisReportData.getSuspect_surname())
                                    .get().addOnSuccessListener(queryDocumentSnapshots -> {
                                if (queryDocumentSnapshots.size() > 0) {
                                    tryGetSuspectPhoto(queryDocumentSnapshots.getDocuments().get(0).get("ID").toString());

                                }


                            })
                            .addOnFailureListener(e -> {

                            });
                        }

                        attachReportListener();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showToast(e.getMessage());
                        onBackPressed();
                    }
                });

            }
        }
//        for (DocumentChange dc : snapshots.getDocumentChanges()) {
//            switch (dc.getType()) {
//
//                case ADDED:
    }


    private void tryGetSuspectPhoto(String suspect_id)
    {
        try {
            StorageReference reference = FirebaseStorage.getInstance().getReference()
                    .child("profileImages")
                    .child(suspect_id + ".jpeg");

            reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    suspectPhotoUrl = uri;

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
//                    imgView.setImageResource(R.drawable.ncst_logo_90px);
                }
            });
        }
        catch (Exception e)
        {

        }
    }

    private void getSenderName(String student_email)
    {
        try {
            if(!MainActivity.instance.cached_student_names.containsKey(student_email))
            {
                Query query = db.collection("students").whereEqualTo("email", student_email);
                query.get().addOnSuccessListener(queryDocumentSnapshots -> {
                    if(queryDocumentSnapshots.size() > 0)
                    {
                        Student me = queryDocumentSnapshots.getDocuments().get(0).toObject(Student.class);
                        MainActivity.instance.cached_student_names.put(me.getEmail(), me.getFirst_name() + " " + me.getLast_name());

                        for(View v : getViewsByTag(recyclerView, me.getEmail()))
                        {
                            ((TextView)v).setText(MainActivity.instance.cached_student_names.get(me.getEmail()));
                            v.setVisibility(View.VISIBLE);
                        }
                    }
                })
                .addOnFailureListener(e -> {
                    // Try get admin name
                    db.collection("users").whereEqualTo("name", student_email)
                    .get().addOnSuccessListener(queryDocumentSnapshots ->
                    {
                        if(queryDocumentSnapshots.size() > 0)
                        {
                            MainActivity.instance.cached_student_names.put(student_email, queryDocumentSnapshots.getDocuments().get(0).get("name").toString());

                            for(View v : getViewsByTag(recyclerView, student_email))
                            {
                                ((TextView)v).setText(MainActivity.instance.cached_student_names.get(student_email));
                                v.setVisibility(View.VISIBLE);
                            }
                        }
                    })
                    .addOnFailureListener(error ->{

                    });
                });
            }
        }
        catch (Exception e)
        {

        }
    }

    private static ArrayList<View> getViewsByTag(ViewGroup root, String tag){
        ArrayList<View> views = new ArrayList<View>();
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = root.getChildAt(i);
            if (child instanceof ViewGroup) {
                views.addAll(getViewsByTag((ViewGroup) child, tag));
            }

            final Object tagObj = child.getTag();
            if (tagObj != null && tagObj.equals(tag)) {
                views.add(child);
            }

        }
        return views;
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
//        fetchMessages();
        fetchOldMessages();
    }

    private boolean hasAlreadyMessage(String key)
    {

        return false;
    }

    private void fetchOldMessages()
    {
        if(oldestVisibleMessage != null && oldestVisibleMessage.getDate("date") != null)
        {
            Query query = db.collection("reports")
                    .document(report_key).collection("messages")
                    .orderBy("date", Query.Direction.DESCENDING)
                    .startAfter(oldestVisibleMessage)
                    .limit(message_count_page);

            if(oldChatListener != null)
                oldChatListener.remove();

            oldChatListener =  query.addSnapshotListener(
                    (snapshots, e) -> {
                        if (e != null) {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            if (user != null) {

                                Log.w(TAG, "listen:error", e);
                                showToast(e.getMessage());
                                mSwipeRefreshLayout.setRefreshing(false);
                            }
                            return;
                        }

                        if(snapshots.size() == 0)
                        {
                            mSwipeRefreshLayout.setRefreshing(false);
                            return;
                        }

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            switch (dc.getType()) {

                                case ADDED:

                                    if(addedMessages.contains(dc.getDocument().getId()))
                                        break;

                                    Message newMessage = dc.getDocument().toObject(Message.class, DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
                                    newMessage.setKey(thisReportData.getKey());
                                    newMessage.setReportId(thisReportData.getReportId());

                                    getSenderName(newMessage.getUser());

                                    Date thisMsgTempDate = dc.getDocument().getDate("date", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
                                    messagesTime.add(thisMsgTempDate.toString());

                                    String dateShown = "";//dont show messages that has the same time
                                    int previousIndex = Math.max(0, currentMessageTimeCheck-1);
                                    if(previousIndex > messagesTime.size()-1)
                                        previousIndex = messagesTime.size()-1;
                                    if(!messagesTime.get(previousIndex).equals(thisMsgTempDate) || previousIndex == 0)

                                    messageList.add(0, newMessage);
                                    oldestVisibleMessage = dc.getDocument();
                                    messageAdapater.notifyItemInserted(0);
                                    recyclerView.scrollToPosition(0);

                                    addedMessages.add(dc.getDocument().getId());

                                    break;

                                case MODIFIED:

                                    break;

                                case REMOVED:

                                    break;
                            }
                        }

                        mSwipeRefreshLayout.setRefreshing(false);
                        messageAdapater.notifyDataSetChanged();
                    });
        }
        else
        {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private void fetchMessages()
    {
        Query query = db.collection("reports")
                .document(report_key).collection("messages")
                .orderBy("date", Query.Direction.DESCENDING)
                .limit(message_count_page);

        if (chatListener != null)
            chatListener.remove();

        chatListener = query.addSnapshotListener(
        (snapshots, e) -> {
            if (e != null) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {

                    Log.w(TAG, "listen:error", e);
                    showToast(e.getMessage());
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                return;
            }

            if(snapshots.size() == 0)
            {
                messageList.clear();

                messageAdapater.notifyDataSetChanged();

                mSwipeRefreshLayout.setRefreshing(false);
                return;
            }

            int i = 0;
            for (DocumentChange dc : snapshots.getDocumentChanges()) {
                switch (dc.getType()) {

                    case ADDED:

                        if(addedMessages.contains(dc.getDocument().getId()))
                            break;


                        Message newMessage = dc.getDocument().toObject(Message.class, DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
                        newMessage.setKey(dc.getDocument().getId());
                        newMessage.setReportId(thisReportData.getReportId());

                        getSenderName(newMessage.getUser());

                        Date thisMsgTempDate = dc.getDocument().getDate("date", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
                        messagesTime.add(thisMsgTempDate.toString());

                        String dateShown = "";//dont show messages that has the same time
                        int previousIndex = Math.max(0, currentMessageTimeCheck-1);
                        if(previousIndex > messagesTime.size()-1)
                            previousIndex = messagesTime.size()-1;
                        if(!messagesTime.get(previousIndex).equals(thisMsgTempDate) || previousIndex == 0)
                            dateShown = thisMsgTempDate.toString();


                        boolean thisIsLater = false;
                        Date thisReportDate = dc.getDocument().getDate("date", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
                        if(messageList.size() > 0) {
                            Message latest = messageList.get(messageList.size() - 1);
                            thisIsLater = (latest.getDate() == null || latest.getDate().before(thisReportDate));
                        }
//                        if(backRead) {

                        if(thisIsLater) {
                            messageList.add(newMessage);
                        }
                        else {
                            messageList.add(0, newMessage);
                            if(oldestVisibleMessage == null || (oldestVisibleMessage != null && oldestVisibleMessage.getDate("date", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE).after(thisReportDate)))
                                oldestVisibleMessage = dc.getDocument();
                        }

                        addedMessages.add(dc.getDocument().getId());


                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                recyclerView.scrollToPosition(messageAdapater.getItemCount() - 1);
                            }
                        }, 100);


                        break;

                    case MODIFIED:

                        break;

                    case REMOVED:

                        break;
                }

                i++;

                if(i >= snapshots.getDocuments().size())
                {
                    mSwipeRefreshLayout.setRefreshing(false);
                    messageAdapater.notifyDataSetChanged();
                }
            }

            mSwipeRefreshLayout.setRefreshing(false);
            messageAdapater.notifyDataSetChanged();
        });
    }



    private void sendMessage()
    {
        String messageText = messageArea.getText().toString();

        if(!messageText.trim().equals("")){
            DateFormat sdf = new SimpleDateFormat("YYYYMMdd MMMM dd hh:mm:ss a");

            Date today = new Date();
//            String current_time = sdf.format(today);

            Message newMessage = new Message(messageText, AuthActivity.instance.loggedStudent.getEmail(), AuthActivity.instance.loggedStudent.getID(), false, "", false);
            newMessage.setKey(thisReportData.getKey());
            newMessage.setReportId(thisReportData.getReportId());

            db.collection("reports").document(report_key).collection("messages").document().set(newMessage)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            if(selectedMedias.size() > 0)
                            {
                                uploadMedias();
                            }
                        }
                    });

//            reference1.push().setValue(newMessage).addOnSuccessListener(new OnSuccessListener<Void>() {
//                @Override
//                public void onSuccess(Void aVoid) {
//
//                    if(selectedMedias.size() > 0)
//                    {
//                        uploadMedias();
//                    }
//
//                }
//            });

//            newMessage.setMsg_date(dateShown);
//
//            int curSize = messageAdapater.getItemCount();
//            messageList.add(newMessage);
//            messageAdapater.notifyItemInserted(curSize);


            //reference2.push().setValue(map);
            messageArea.setText("");
        }
        else//allow to send blank message if there's an attachment
        {
            if(selectedMedias.size() > 0)
            {

//                        uploadsListView.setAdapter(uploadsAdapter);
                //upload medias
                uploadMedias();
            }
        }
    }

    private void uploadMedias()
    {
        my_uploads_layout.setVisibility(View.VISIBLE);
        my_chatbox_layout.setVisibility(View.GONE);
        Log.d(TAG, "begin uploading medias");
        skippedFiles.clear();
        uploadingTasks.clear();
        
        List<String> selectedMediasCopy = new ArrayList<>(selectedMedias);
        selectedMedias.clear();

        //temporary arrays to store data for service intent
        String[] array_uri = new String[selectedMediasCopy.size()];
        String[] array_filename = new String[selectedMediasCopy.size()];
        String[] array_originalFileName = new String[selectedMediasCopy.size()];
        long[] array_fileSize = new long[selectedMediasCopy.size()];

//        for (String uri: selectedMediasCopy) {
        for(int i=0; i < selectedMediasCopy.size(); i ++)
        {
            String uri = selectedMediasCopy.get(i);


            String[] splitUri = uri.split("\\.");
            String filenameOnly = UUID.randomUUID() + "." +splitUri[splitUri.length-1];

            Log.d(TAG, "processing file " + filenameOnly);

            splitUri = uri.split("/");
            String origFilenameOnly = splitUri[splitUri.length-1];
            
            ContentResolver cr = getContentResolver();
            InputStream is = null;

            File file = new File(uri);
            int fileSize = 0;

//            try {
//                is = cr.openInputStream(Uri.fromFile(file));
//                cr.getType(Uri.fromFile(file));
//                int size = is.available();
                //Log.d(TAG, "Video file size is : " + MyUtilMedia.getFileSize(size));

                fileSize = Integer.parseInt(String.valueOf(file.length()/1024));;//MyUtilMedia.getFileSize(size);

                if(fileSize > MyUtilMedia.videoFileSizeLimit)
                {
                    skippedFiles.add(origFilenameOnly + " " + Formatter.formatShortFileSize(context, file.length()));
                    if(skippedFiles.size() >= selectedMediasCopy.size())
                        checkIfAllUploaded();
                    continue;
                }

//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }


            //continueUpload(uri, filenameOnly, origFilenameOnly, fileSize);

            array_uri[i] = uri;
            array_filename[i] = filenameOnly;
            array_originalFileName[i] = origFilenameOnly;
            array_fileSize[i] = fileSize;


            if(i == (selectedMediasCopy.size()-1))
            {
//                if(isArrayEmpty(array_uri)) {
//                    checkIfAllUploaded();
//                }
//                else
//                {
                    Intent mServiceIntent = new Intent(Chat.this, MyUploadService.class);
                    mServiceIntent.putExtra("array_uri", array_uri);
                    mServiceIntent.putExtra("array_filename", array_filename);
                    mServiceIntent.putExtra("array_originalFileName", array_originalFileName);
                    mServiceIntent.putExtra("array_fileSize", array_fileSize);
                    mServiceIntent.putExtra("report_id", report_id);
                    mServiceIntent.putExtra("report_key", report_key);

                    startService(mServiceIntent);
//                }
            }
//            i++;
        }

        Log.d(TAG, "done iterating all files");
    }

    private boolean isArrayEmpty(Object[] array)
    {
        for (Object ob : array) {
            if (ob != null) {
                return false;
            }
        }
        return true;
    }


    public void uploadVideoThumbnail(String uri, String filename)
    {
        //create video thumbnail then upload it on firebase thumbnail folder

        //change file extension of video thumbnail
        String[] temp = filename.split("\\.");
        filename = temp[0] + ".jpg";

        String finalFilename = filename;
        SimpleTarget target = new SimpleTarget<Bitmap>() {

            @Override
            public void onResourceReady(@NonNull Bitmap theBitmap, @Nullable Transition<? super Bitmap> transition) {


                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    theBitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                    byte[] byteArray = baos.toByteArray();

                    StorageReference reference = FirebaseStorage.getInstance().getReference()
                            .child("video_thumbnails/" + report_id + "/" + finalFilename);
                    Log.d(TAG, "creating video thumbnail for : " + finalFilename);

                    reference.putBytes(byteArray).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        }
                    });
            }
        };

        if(context != null) {
            Glide
                    .with(context)
                    .asBitmap()
                    .load(Uri.fromFile(new File(uri)))
                    .into(target);
        }
    }

    public void updateMetaData(String filename, String mimeType)
    {
        StorageMetadata metadata = new StorageMetadata.Builder()
                .setContentType(mimeType)
                .build();

        // Update metadata properties
        StorageReference forestRef = FirebaseStorage.getInstance().getReference().child("reports/" + report_id + "/" + filename);
        forestRef.updateMetadata(metadata)
                .addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                    @Override
                    public void onSuccess(StorageMetadata storageMetadata) {
                        // Updated metadata is in storageMetadata
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Uh-oh, an error occurred!
                    }
                });
    }

    public void cancelUpload(String uploadFileName, String origFilename)
    {
        UploadTask uploadTask = uploadingTasks.get(uploadFileName);

        if(uploadTask != null)
        {
            uploadTask.cancel();
        }
    }

    public void updateProgressUpload(String filenameOnly, UploadTask.TaskSnapshot taskSnapshot)
    {
        String totalProgressText = MyUtilMedia.getProperFileSize(taskSnapshot.getBytesTransferred()) + "/" + MyUtilMedia.getProperFileSize(taskSnapshot.getTotalByteCount());
//        TextView txt = getFileSizeViewByFileName(filenameOnly);
//        if(txt != null)
        txt_current_upload_name.setText(filenameOnly);

//        ProgressBar progressBar = getProgressBarViewByFileName(filenameOnly);
//        if(progressBar != null)
//        {
            progressBar.setMax((int) taskSnapshot.getTotalByteCount());
            progressBar.setProgress((int) taskSnapshot.getBytesTransferred());
//        }
    }

//    public TextView getFileSizeViewByFileName(String filename)
//    {
//        Log.d(TAG, " adapter count : " + uploadsAdapter.getCount());
//        Log.d(TAG, allUploadViews.keySet().toString());
//        View tryView = allUploadViews.get(filename);
//
//        if(tryView != null)
//            return tryView.findViewById(R.id.txt_upload_filesize);
//        else
//            return null;
//    }

//    public ProgressBar getProgressBarViewByFileName(String filename)
//    {
//        Log.d(TAG, allUploadViews.keySet().toString());
//
//        View tryView = allUploadViews.get(filename);
//
//        if(tryView != null)
//            return tryView.findViewById(R.id.upload_progressbar);
//        else
//            return null;
//    }

    public void checkIfAllUploaded()
    {
        if(uploadingTasks.isEmpty())
        {
            if(skippedFiles.size() > 0) {
//                String message = "Some media files are skipped because they exceed the file size limit(" + MyUtilMedia.videoFileSizeLimit + "MB), or video duration (" + MyUtilMedia.videoFileDurationLimit + ")secs \n";
                String message = "Some media files are skipped because they exceed the file size limit(" + MyUtilMedia.videoFileSizeLimit + "MB) \n";

                for (String ss: skippedFiles) {
                    message += ss + " \n";
                }


                showUploadSummaryDialog("Upload Complete", message);
            }
            else
            {

            }

//            uploadList.clear();
//            allUploadViews = new HashMap<>();
            my_uploads_layout.setVisibility(View.GONE);
            my_chatbox_layout.setVisibility(View.VISIBLE);
            setCounterText("");
        }
    }

    private void setCounterText(String txt)
    {
        if(txt.length() > 0)
            media_files_counter.setVisibility(View.VISIBLE);
        else
            media_files_counter.setVisibility(View.GONE);

        media_files_counter.setText(txt);
    }

    private void startPixImagePicker()
    {
        Options options = Options.init()
                .setRequestCode(pixImageRequest)                                           //Request code for activity results
                .setCount(MyUtilMedia.maxFileUploadCount)                                                   //Number of images to restict selection count
                .setFrontfacing(false)                                         //Front Facing camera on start
                .setPreSelectedUrls(selectedMedias)                               //Pre selected Image Urls
                .setExcludeVideos(false)                                       //Option to exclude videos
                .setVideoDurationLimitinSeconds(MyUtilMedia.videoFileDurationLimit)                            //Duration for video recording
                .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)     //Orientaion
                .setPath("/storage/emulated/0/DCIM/");                                       //Custom Path For media Storage

        PixCustom.start(Chat.this, options);
    }

//    private void loadImageFromURL(Uri url, ImageView imageView)
//    {
//        SimpleTarget target = new SimpleTarget<Bitmap>() {
//
//            @Override
//            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                imageView.setImageBitmap(resource);
//                Log.d(TAG, "done placing image from url");
//            }
//        };
//
//        Glide.with(this).asBitmap().load(url).placeholder(R.drawable.noimage)
//                .error(R.drawable.noimage)
//                .into(target);
//    }
//
//    private void loadProfile(Uri url, ImageView imgView) {
//        SimpleTarget target = new SimpleTarget<Bitmap>() {
//
//            @Override
//            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                imgView.setImageBitmap(resource);
//                MainActivity.instance.addBitmapToMemoryCache(AuthActivity.instance.loggedStudent.getID(), getBitmapFromView(imgView));
//                Log.d(TAG, "done placing glide");
//            }
//        };
//
//        Glide.with(this).asBitmap().load(url).placeholder(R.drawable.noimage)
//                .error(R.drawable.noimage)
//                .into(target);
//    }

//    public void getChatHeadImage(ImageView imgView, String senderID)
//    {
//
//        if(cached_profile_image_urls.containsKey(senderID))
//        {
//            Glide.with(getApplicationContext())
//                    .load(cached_profile_image_urls.get(senderID))
//                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
//                    .into(imgView);
//        }
//        else
//        {
//            StorageReference reference = FirebaseStorage.getInstance().getReference()
//                    .child("profileImages")
//                    .child(senderID + ".jpeg");
//
//            reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//                @Override
//                public void onSuccess(Uri uri) {
//                    Glide.with(getApplicationContext())
//                            .load(uri)
//                            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
//                            .into(imgView);
//                    cached_profile_image_urls.put(senderID, uri);
//
//                }
//            }).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception e) {
//                    imgView.setImageResource(R.drawable.ncst_logo_90px);
//                }
//            });
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == pixImageRequest) {
            selectedMedias = data.getStringArrayListExtra(PixCustom.IMAGE_RESULTS);

//            selectedMedias = UtilsData.removeDuplicates(selectedMedias);
            if(selectedMedias.size() > 0) {
                setCounterText(selectedMedias.size() + " media file" + ((selectedMedias.size() > 1) ? "s" : "") + " selected.");
            }
            else {
                setCounterText("");
            }

            for (String str: selectedMedias) {
                Log.d(TAG, str);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startPixImagePicker();
                } else {
                    Toast.makeText(Chat.this, "Approve permissions to open Pix ImagePicker", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }



    private void showUploadSummaryDialog(String title, String message)
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(message).setPositiveButton("Ok", null).show();
    }


    private void showToast(String message){
        Toast.makeText(MainActivity.myContext, message, Toast.LENGTH_LONG).show();
    }

    public Bitmap getBitmapFromView(View view)
    {
        Log.d(TAG, "imgWidth = " + view.getWidth());
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
//            super.onBackPressed();
//            //additional code
//            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra("EXTRA_GOTO_FRAGMENT", UtilFragments.ReportsFragment);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else {
            getSupportFragmentManager().popBackStack();
//            getSupportActionBar().show();
        }
    }
    public void preventClicks(View view) {}
}