package com.telbound.orca;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerListener;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.telbound.fragments.main.ReportsFragment;
import com.telbound.models.Category;
import com.telbound.models.Message;
import com.telbound.models.MyMultiSearch;
import com.telbound.models.PersonDescriptions;
import com.telbound.models.Report;
import com.telbound.utils.FetchServerTimeTask;
import com.telbound.utils.MyCallbackInterface;
import com.telbound.utils.UtilFragments;

import net.alhazmy13.mediapicker.Image.ImagePicker;
import net.alhazmy13.mediapicker.Video.VideoPicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import jp.wasabeef.richeditor.RichEditor;
import yuku.ambilwarna.AmbilWarnaDialog;

import static android.view.View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION;

public class ComposerActivity extends AppCompatActivity {

    private static final String TAG = "myLogTag";
    private TextView counter;

    private ArrayList<String> selectedMediaPaths;
    private List<KeyPairBoolData> keyCategories;
    private List<KeyPairBoolData> keyDescription;
    private List<String> selectedCategoryList;
    private List<String> selectedDescriptionList;

    private StorageReference mStorageRef;

    private FirebaseFirestore db;
    private ListenerRegistration categoriesListener;
    private ListenerRegistration descriptionListener;

    public static PersonDescriptions personDescriptions;

    private HashMap<String, Category> cached_categories = new HashMap<>();
    private TextView mPreview;
    private RichEditor mEditor;
    private int pressedColor, normalColor;
    private int editorBGColor;
    private AmbilWarnaDialog colorDialog;
    private AmbilWarnaDialog backgroundColorDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_composer);

        pressedColor = getResources().getColor(R.color.colorAccent);
        normalColor = getResources().getColor(R.color.black);

        db = FirebaseFirestore.getInstance();

        Toolbar myToolbar = findViewById(R.id.main_toolbar);

        SpannableString s = new SpannableString("Create New Report");
        s.setSpan(new TypefaceSpan("open_sans_bold.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setTitle(s);
        setSupportActionBar(myToolbar);


        selectedMediaPaths = new ArrayList<>();
        keyCategories = new ArrayList<>();
        keyDescription = new ArrayList<>();
        selectedCategoryList = new ArrayList<>();
        selectedDescriptionList = new ArrayList<>();

        mStorageRef = FirebaseStorage.getInstance().getReference();
        //fetchCategories();


        fetchCategories();
        initControls();
    }

    private void fetchCategories()
    {
        if (categoriesListener != null)
            categoriesListener.remove();

        keyCategories.clear();
        cached_categories.clear();

        categoriesListener = db.collection("categories").addSnapshotListener(
                (snapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "listen:error", e);
                        showToast(e.getMessage());
                        progressBar(false);
                        return;
                    }

                    if(snapshots.getDocuments().size() == 0)
                    {
                        showToast("There are no categories yet.");
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.frameContainer, new ReportsFragment(),
                                        UtilFragments.HomeFragment).commit();
                        progressBar(false);
                        return;
                    }


                    List<DocumentChange> documentChanges = snapshots.getDocumentChanges();
                    for (int i = 0; i < documentChanges.size(); i++) {
                        DocumentChange dc = documentChanges.get(i);
                        switch (dc.getType()) {

                            case ADDED:

                                Category thisCategory = dc.getDocument().toObject(Category.class);
                                if(!cached_categories.containsKey(thisCategory.getName())) {
                                    KeyPairBoolData h = new KeyPairBoolData();
                                    h.setId(i);
                                    h.setName((String) dc.getDocument().get("name"));
                                    h.setSelected(false);
                                    keyCategories.add(h);

//                                HashMap<String, String> thisCategoryHashmap = new HashMap<>();
//                                thisCategoryHashmap.put("name", (String) dc.getDocument().get("name"));
//                                thisCategoryHashmap.put("color", (String) dc.getDocument().get("color"));

                                    cached_categories.put(thisCategory.getName(), thisCategory);
                                }

                                break;
                            case MODIFIED:
                                break;
                            case REMOVED:
                                break;

                        }
                    }

                    progressBar(false);
                    fetchDescriptions();
                }
        );
    }

    private void showToast(String text)
    {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
    }

    private void fetchDescriptions()
    {
        progressBar(true);

        if (descriptionListener != null)
            descriptionListener.remove();


        descriptionListener = db.collection("descriptions").addSnapshotListener(
                (snapshots, e) -> {
                    if (e != null) {
                        Log.w(TAG, "listen:error", e);
                        showToast(e.getMessage());
                        progressBar(false);
                        return;
                    }

                    if(snapshots.getDocuments().size() == 0)
                    {
                        showToast("There are no descriptions yet.");
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.frameContainer, new ReportsFragment(),
                                        UtilFragments.HomeFragment).commit();
                        progressBar(false);
                        return;
                    }

                    personDescriptions = new PersonDescriptions();

                    List<DocumentChange> documentChanges = snapshots.getDocumentChanges();
                    for (int i = 0; i < documentChanges.size(); i++) {
                        DocumentChange dc = documentChanges.get(i);
                        switch (dc.getType()) {

                            case ADDED:

                                personDescriptions.descriptions.put(dc.getDocument().getId(), (List<String>) dc.getDocument().get("opposite"));

                                KeyPairBoolData h = new KeyPairBoolData();
                                h.setId(i);
                                h.setName(dc.getDocument().getId());
                                h.setSelected(false);
                                keyDescription.add(h);

                                break;
                            case MODIFIED:
                                break;
                            case REMOVED:
                                break;

                        }
                    }

                    progressBar(false);
//                    fetchDescriptions();
                }
        );
    }

    @SuppressLint("DefaultLocale")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case VideoPicker.VIDEO_PICKER_REQUEST_CODE:
                    List<String> itemVideo = Objects.requireNonNull(data).getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH);
                    selectedMediaPaths.add(
                            Objects.requireNonNull(itemVideo).toString().substring(1, itemVideo.toString().length() - 1)
                    );

                    break;
                case ImagePicker.IMAGE_PICKER_REQUEST_CODE:
                    List<String> itemImage = Objects.requireNonNull(data).getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
                    selectedMediaPaths.add(
                            Objects.requireNonNull(itemImage).toString().substring(1, itemImage.toString().length() - 1)
                    );

                    break;
                default:
                    break;
            }

            counter.setText(String.format("%d %s", Objects.requireNonNull(selectedMediaPaths).size(),
                    (selectedMediaPaths.size() == 1) ? getString(R.string.attachment_counter_singular) :
                            getString(R.string.attachment_counter_plural)));
        }
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(this));
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", TAG, null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void togglePressedIcon(View view)
    {
        int color = normalColor;
        Drawable background = view.getBackground();
        if (background instanceof ColorDrawable)
            color = ((ColorDrawable) background).getColor();

        if(color == normalColor)
            view.setBackgroundColor(pressedColor);
        else
            view.setBackgroundColor(normalColor);
    }

    private void initEditor() {


        mEditor = findViewById(R.id.report_message);

        mEditor.setEditorHeight(200);
        mEditor.setEditorFontSize(22);
        mEditor.setEditorFontColor(Color.BLACK);

        editorBGColor = Color.TRANSPARENT;





        //mEditor.setEditorBackgroundColor(Color.BLUE);
        //mEditor.setBackgroundColor(Color.BLUE);
        //mEditor.setBackgroundResource(R.drawable.bg);
        mEditor.setPadding(10, 10, 10, 10);
        //mEditor.setBackground("https://raw.githubusercontent.com/wasabeef/art/master/chip.jpg");
        mEditor.setPlaceholder("Insert message here...");
        //mEditor.setInputEnabled(false);

//        mPreview = (TextView) findViewById(R.id.preview);
        mEditor.setOnTextChangeListener(new RichEditor.OnTextChangeListener() {
            @Override public void onTextChange(String text) {
//                mPreview.setText(text);
            }
        });

        findViewById(R.id.action_undo).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.undo();
            }
        });

        findViewById(R.id.action_redo).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.redo();
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_bold).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setBold();
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_italic).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setItalic();
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_subscript).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setSubscript();
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_superscript).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setSuperscript();
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_strikethrough).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setStrikeThrough();
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_underline).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setUnderline();
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_heading1).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setHeading(1);
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_heading2).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setHeading(2);
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_heading3).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setHeading(3);
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_heading4).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setHeading(4);
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_heading5).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setHeading(5);
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_heading6).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setHeading(6);
                togglePressedIcon(v);
            }
        });

        findViewById(R.id.action_txt_color).setOnClickListener(new View.OnClickListener() {
//            private boolean isChanged;

            @Override public void onClick(View v) {
                colorDialog = new AmbilWarnaDialog(ComposerActivity.this, mEditor.getSolidColor(), new AmbilWarnaDialog.OnAmbilWarnaListener() {
                    @Override
                    public void onOk(AmbilWarnaDialog dialog, int color) {
                        // color is the color selected by the user.
                        mEditor.setTextColor(color);
                    }

                    @Override
                    public void onCancel(AmbilWarnaDialog dialog) {
                        // cancel was selected by the user
                    }
                });
                colorDialog.show();
//                isChanged = !isChanged;
            }
        });

        findViewById(R.id.action_bg_color).setOnClickListener(new View.OnClickListener() {
//            private boolean isChanged;

            @Override public void onClick(View v) {
//                mEditor.setTextBackgroundColor(isChanged ? Color.TRANSPARENT : Color.YELLOW);
//                isChanged = !isChanged;
                backgroundColorDialog = new AmbilWarnaDialog(ComposerActivity.this, editorBGColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                    @Override
                    public void onOk(AmbilWarnaDialog dialog, int color) {
                        // color is the color selected by the user.
                        editorBGColor = color;
                        mEditor.setBackgroundColor(color);
                    }

                    @Override
                    public void onCancel(AmbilWarnaDialog dialog) {
                        // cancel was selected by the user
                    }
                });

                backgroundColorDialog.show();
            }
        });

        findViewById(R.id.action_indent).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setIndent();
            }
        });

        findViewById(R.id.action_outdent).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setOutdent();
            }
        });

        findViewById(R.id.action_align_left).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setAlignLeft();
            }
        });

        findViewById(R.id.action_align_center).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setAlignCenter();
            }
        });

        findViewById(R.id.action_align_right).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setAlignRight();
            }
        });

        findViewById(R.id.action_blockquote).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setBlockquote();
            }
        });

        findViewById(R.id.action_insert_bullets).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setBullets();
            }
        });

        findViewById(R.id.action_insert_numbers).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.setNumbers();
            }
        });

        findViewById(R.id.action_insert_image).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.insertImage("http://www.1honeywan.com/dachshund/image/7.21/7.21_3_thumb.JPG",
                        "dachshund");
            }
        });

        findViewById(R.id.action_insert_link).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.insertLink("https://github.com/wasabeef", "wasabeef");
            }
        });
        findViewById(R.id.action_insert_checkbox).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mEditor.insertTodo();
            }
        });
    }

    private void initControls() {
        MultiSpinnerSearch categories = findViewById(R.id.report_categories);

//        EditText message = findViewById(R.id.report_message);



        initEditor();

        Button send = findViewById(R.id.btn_send);

        //Custom
        MyMultiSearch suspect_description = findViewById(R.id.suspect_description);
        EditText suspect_name = findViewById(R.id.suspect_name);
        EditText suspect_surname = findViewById(R.id.suspect_surname);

        CheckBox suspect_toggle = findViewById(R.id.suspect_toggle);
        LinearLayout suspect_name_layout = findViewById(R.id.suspect_name_layout);
        LinearLayout suspect_surname_layout = findViewById(R.id.suspect_surname_layout);
        LinearLayout suspect_description_layout = findViewById(R.id.suspect_description_layout);

        RadioGroup gender_radio = findViewById(R.id.gender_radio);


        //toggle I know the suspect
        suspect_toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
               @Override
               public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                   if(isChecked)
                   {
                        suspect_description_layout.setVisibility(View.GONE);
                        suspect_name_layout.setVisibility(View.VISIBLE);
                        suspect_surname_layout.setVisibility(View.VISIBLE);
                   }
                   else {
                       suspect_name_layout.setVisibility(View.GONE);
                       suspect_surname_layout.setVisibility(View.GONE);
                       suspect_description_layout.setVisibility(View.VISIBLE);
                   }
               }
           }
        );

        counter = findViewById(R.id.attachment_count);

//        attach_videos.setOnClickListener(v -> Dexter.withActivity(ComposerActivity.this)
//                .withPermissions(
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                        Manifest.permission.READ_EXTERNAL_STORAGE)
//                .withListener(new MultiplePermissionsListener() {
//                    @Override
//                    public void onPermissionsChecked(MultiplePermissionsReport report) {
//                        if (report.areAllPermissionsGranted()) {
//                            new VideoPicker.Builder(ComposerActivity.this)
//                                    .mode(VideoPicker.Mode.GALLERY)
//                                    .directory(VideoPicker.Directory.DEFAULT)
//                                    .extension(VideoPicker.Extension.MP4)
//                                    .enableDebuggingMode(true)
//                                    .build();
//                        }
//
//                        if (report.isAnyPermissionPermanentlyDenied()) {
//                            showSettingsDialog();
//                        }
//                    }
//
//                    @Override
//                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//                        token.continuePermissionRequest();
//                    }
//                }).check());
//
//        attach_images.setOnClickListener(v -> Dexter.withActivity(ComposerActivity.this)
//                .withPermissions(
//                        Manifest.permission.CAMERA,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                        Manifest.permission.READ_EXTERNAL_STORAGE)
//                .withListener(new MultiplePermissionsListener() {
//                    @Override
//                    public void onPermissionsChecked(MultiplePermissionsReport report) {
//                        if (report.areAllPermissionsGranted()) {
//                            new ImagePicker.Builder(ComposerActivity.this)
//                                    .mode(ImagePicker.Mode.GALLERY)
//                                    .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
//                                    .directory(ImagePicker.Directory.DEFAULT)
//                                    .extension(ImagePicker.Extension.PNG)
//                                    .scale(600, 600)
//                                    .allowMultipleImages(true)
//                                    .enableDebuggingMode(true)
//                                    .build();
//                        }
//
//                        if (report.isAnyPermissionPermanentlyDenied()) {
//                            showSettingsDialog();
//                        }
//                    }
//
//                    @Override
//                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//                        token.continuePermissionRequest();
//                    }
//                }).check());

        categories.setEmptyTitle(getString(R.string.composer_category_not_found));
        categories.setItems(keyCategories, new MultiSpinnerListener() {
            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).isSelected()) {
//                        Log.i(TAG, i + " : " + items.get(i).getName() + " : " + items.get(i).isSelected());
                        selectedCategoryList.add(items.get(i).getName());
                    }
                }
            }
        });

//        categories.setItems(keyCategories, -1, items -> {
//            for (int i = 0; i < items.size(); i++) {
//                if (items.get(i).isSelected()) {
//                    //Log.e(TAG, i + " : " + items.get(i).getId() + " : " + items.get(i).isSelected());
//                    selectedCategoryList.add(items.get(i).getName());
//                }
//            }
//        });


        suspect_description.setEmptyTitle(getString(R.string.composer_category_not_found));
        suspect_description.setItems(keyDescription, -1, items -> {
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).isSelected()) {
                    Log.e(TAG, i + " : " + items.get(i).getId() + " : " + items.get(i).isSelected());
                    selectedDescriptionList.add(items.get(i).getName());
                }
            }
        });

        send.setOnClickListener(v -> {
            if (categories.getSelectedItems().size() == 0 || (!suspect_toggle.isChecked() && suspect_description.getSelectedItems().size() == 0) || TextUtils.isEmpty(mEditor.getHtml()) || (suspect_toggle.isChecked() && (suspect_name.getText().toString().isEmpty() && suspect_surname.getText().toString().isEmpty())))  {
                Toast.makeText(ComposerActivity.this, R.string.composer_all_fields_are_required,
                        Toast.LENGTH_LONG).show();
            } else {
//                sendVolleyPostRequest(message.getText().toString(),
//                        selectedCategoryList.toString().substring(1, selectedCategoryList.toString().length() - 1),
//                        selectedMediaPaths);
                String gender = "male";
                if(gender_radio.getCheckedRadioButtonId() == R.id.radio_female)
                    gender = "female";

                String suspectName = "";
                String suspectSurName = "";
                List<String> descriptions = new ArrayList<>();

                if(suspect_toggle.isChecked()) {
                    suspectName = suspect_name.getText().toString();
                    suspectSurName = suspect_surname.getText().toString();

                    if(suspectName.isEmpty() || suspectSurName.isEmpty())
                    {
                        showToast("Both suspect first name and last name should be provided.");
                        return;
                    }

                }
                else {
                    descriptions = selectedDescriptionList;//.toString().substring(1, selectedDescriptionList.toString().length() - 1);
                }

//                List<String> categoryString = selectedCategoryList;//.toString().substring(1, selectedCategoryList.toString().length() - 1);
//                Log.d(TAG, categoryString);

                SendReport(suspectName.trim().replaceAll(" +", " "), suspectSurName.trim().replaceAll(" +", " "), mEditor.getHtml(), gender, selectedCategoryList, selectedMediaPaths, descriptions);
            }
        });
    }



    private void JoinReport(String docKey, String reportId, String content, List<String> current_senders, List<Category> selectedCategories, List<String> selectedDescriptions, ArrayList<String> mediaPaths)
    {
//        Report reportOverride = new Report();
        if(!current_senders.contains(AuthActivity.instance.loggedStudent.getEmail()))
            current_senders.add(AuthActivity.instance.loggedStudent.getEmail());

//        db.collection("reports").document(docKey).update("categories", FieldValue.arrayUnion(selectedCategories.toArray()), "descriptions", FieldValue.arrayUnion(selectedDescriptions), "senders", FieldValue.arrayUnion(current_senders))
        db.collection("reports").document(docKey).update("descriptions", FieldValue.arrayUnion(selectedDescriptions.toArray()), "senders", FieldValue.arrayUnion(current_senders.toArray()))
//        db.collection("reports").document(docKey).set(reportOverride, SetOptions.mergeFields(fieldOverrides))
                .addOnSuccessListener(aVoid -> {
                    db.collection("reports").document(docKey).update("categories", FieldValue.arrayUnion(selectedCategories.toArray()))
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Message newMessage = new Message(content, AuthActivity.instance.loggedStudent.getEmail(), AuthActivity.instance.loggedStudent.getID(), false, "", false);
                                    newMessage.setKey(docKey);
                                    newMessage.setReportId(reportId);
                                    newMessage.setInitial(true);

                                    db.collection("reports").document(docKey).collection("messages").document().set(newMessage).addOnSuccessListener((OnSuccessListener<Void>) aVoid1 -> {

                                        Intent i = new Intent(ComposerActivity.this, Chat.class);
                                        i.putExtra("EXTRA_REPORT_ID", reportId);
                                        i.putExtra("EXTRA_REPORT_KEY", docKey);
                                        Log.d(TAG, docKey + " set EXTRA");
                                        i.putStringArrayListExtra("upload_medias", (ArrayList<String>) mediaPaths);
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i);

                                    }).addOnFailureListener((OnFailureListener) e -> {
                                        progressBar(false);
                                        showToast(e.getMessage());

                                    });
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    progressBar(false);
                                    showToast(e.getMessage());
                                }
                            });

        }).addOnFailureListener(e -> {
            progressBar(false);
            showToast(e.getMessage());
        });
    }

    // This will run evertime a user posted a new report, and this will make sure to reset the report count back to zero
    private void checkResetTodaysReportCount()
    {
        DocumentReference serverTimeRef = db.collection("servertime").document("current");

        serverTimeRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot document = task.getResult();

                if(document.exists())
                {
                    Date serverDate = document.getDate("servertime", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
                    continueCheckResetTodaysReportCount(serverDate);
                }
                else
                {
                    db.collection("servertime").document("current")
                            .update("servertime", FieldValue.serverTimestamp())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    checkResetTodaysReportCount();
                                }
                            });
                }
            }
        });
    }

    private void continueCheckResetTodaysReportCount(Date serverDate)
    {
        DocumentReference reportCountRef = db.collection("TodaysReport").document("current");

        reportCountRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot documentSnapshot = task.getResult();

                if(documentSnapshot.exists())
                {
                    Date lastReportDate = documentSnapshot.getDate("timestamp", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
                    Calendar cal1 = Calendar.getInstance();
                    Calendar cal2 = Calendar.getInstance();
                    cal1.setTime(serverDate);
                    cal2.setTime(lastReportDate);
                    boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                            cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);

                    if(!sameDay)
                    {
                        reportCountRef.update("reportCount", 0, "timestamp", FieldValue.serverTimestamp());
                    }
                }
                else
                {
                    reportCountRef.update("reportCount", FieldValue.increment(1), "timestamp", FieldValue.serverTimestamp())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            continueCheckResetTodaysReportCount(serverDate);
                        }
                    });
                }
            }
        });
    }

    //define callback interface


    //your method slightly modified to take callback into account


    //your async task class
//    private class FetchServerTimeTask extends AsyncTask<String, Void, Date> {
//
//        final MyCallbackInterface callback;
//
//        FetchServerTimeTask(MyCallbackInterface callback) {
//            this.callback = callback;
//            Log.d(TAG, "onInstantiate");
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            Log.d(TAG, "onPreExecute");
//
//            DocumentReference serverTimeRef = db.collection("servertime").document("current");
//
//            serverTimeRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
//                @Override
//                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
//                    DocumentSnapshot document = task.getResult();
//
//                    if(document.exists())
//                    {
//                        Date serverDate = document.getDate("servertime", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
//                        callback.doneFetchingServerTime(serverDate);
//                    }
//                    else
//                    {
//                        db.collection("servertime").document("current")
//                        .update("servertime", FieldValue.serverTimestamp())
//                        .addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
////                                checkResetTodaysReportCount();
//                                onPreExecute();
//
//                            }
//                        });
//                    }
//                }
//            });
//        }
//
//        @Override
//        protected Date doInBackground(String... strings) {
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Date result) {
//            Log.d(TAG, "onPostExecute");
//        }
//
//        //except for this leave your code for this class untouched...
//    }

    public void startFetchServerTime(MyCallbackInterface callback) {
        new FetchServerTimeTask(callback).execute();
    }

    private void SendNewReport(String suspect_first_name, String suspect_last_name, String content, String gender, List<Category> selectedCategories, List<String> mediaPaths, List<String> selectedDescriptions)
    {
        startFetchServerTime(new MyCallbackInterface() {

            @Override
            public void doneFetchingServerTime(Date serverDate) {
                // Do something when download finished
                DocumentReference reportCountRef = db.collection("TodaysReport").document("current");

                OnCompleteListener completeListener = (OnCompleteListener<Void>) task1 -> reportCountRef
                .get().addOnSuccessListener(documentSnapshot -> {
                    if(!documentSnapshot.exists())
                    {
                        progressBar(false);
                        return;
                    }

                    String str = String.format("%03d", documentSnapshot.get("reportCount"));


                    SimpleDateFormat sdf = new SimpleDateFormat("E MMMM dd yyyy hh:mm:ss a");
//                              Date today = documentSnapshot.getDate("timestamp", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);

                    Calendar calendar = new GregorianCalendar();
                    calendar.setTime(serverDate);
                    int year = calendar.get(Calendar.YEAR);
                    //Add one to month {0 - 11}
                    int month = calendar.get(Calendar.MONTH) + 1;
                    int day = calendar.get(Calendar.DAY_OF_MONTH);
                    String preNewReportID = "CASE#" + year + month + (day < 10? "0" : "") + day;

                    String newReportID = preNewReportID + "_" + str;

                    List<String> senders = new ArrayList<>();
                    senders.add(AuthActivity.instance.loggedStudent.getEmail());

                    Report newReport = new Report(newReportID, sdf.format(serverDate), selectedCategories, selectedDescriptions, senders, suspect_first_name.toLowerCase(), suspect_last_name.toLowerCase(), gender, 3);
                    DocumentReference newDoc = db.collection("reports").document();

                    newReport.key = newDoc.getId();

                    newDoc.set(newReport).addOnSuccessListener(aVoid1 -> {

                        List<String> categoryNames = new ArrayList<>();
                        for(Category c : selectedCategories)
                        {
                            categoryNames.add(c.getName());
                        }

                        SimpleDateFormat dateFormat_DateOnly = new SimpleDateFormat("EEE MMMM dd yyyy");
                        SimpleDateFormat dateFormat_timeOnly = new SimpleDateFormat("hh:mm aa");

                        Map<String, Object> newProcessData = new HashMap<>();
                        newProcessData.put("name", "Initial Report");
                        newProcessData.put("message", "<p><b>Victim:</b> " + AuthActivity.instance.loggedStudent.getFirst_name() + " " + AuthActivity.instance.loggedStudent.getLast_name() + "</p><br>"
                        + "<p><b>Report Category:</b>" +categoryNames.toString().replaceAll("\\[", "").replaceAll("\\]","")  + "</p><br>"
                        + "<p><b>Date:</b>" + dateFormat_DateOnly.format(serverDate) + "</p><br>"
                        + "<p><b>Time:</b>" + dateFormat_timeOnly.format(serverDate) + "</p><br>"
                        +"<p><b>Message:</b>" + content + "</p>"
                        );
                        newProcessData.put("timestamp", FieldValue.serverTimestamp());


                        newDoc.collection("Process").document().set(newProcessData, SetOptions.merge())

//                        newDoc.collection("messages").document().set( newMessage, SetOptions.merge())
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid1) {

//                                Intent i = new Intent(ComposerActivity.this, Chat.class);
//                                i.putExtra("EXTRA_REPORT_ID", newReportID);
//                                i.putExtra("EXTRA_REPORT_KEY", newReport.key);
//                                i.putStringArrayListExtra("upload_medias", (ArrayList<String>) mediaPaths);
//                                startActivity(i);

                                finish();
                            }
                        }).addOnFailureListener(e -> {
                            progressBar(false);
                            showToast(e.getMessage());
                        });


                    }).addOnFailureListener(e -> {
                        progressBar(false);
                        showToast(e.getMessage());
                    });
                })
                .addOnFailureListener(e -> {
                    progressBar(false);
                    showToast(e.getMessage());
                });




                reportCountRef.get().addOnCompleteListener(task -> {
                    DocumentSnapshot lastReportDocumentSnapshot = task.getResult();

                    if(!task.isSuccessful())
                    {
                        reportCountRef.update("reportCount", 1, "timestamp", FieldValue.serverTimestamp())
                                .addOnCompleteListener(completeListener);
                        return;
                    }
//                    int current_today_report_count = 0;

                    if(lastReportDocumentSnapshot.exists())
                    {
                        Date lastReportDate = lastReportDocumentSnapshot.getDate("timestamp", DocumentSnapshot.ServerTimestampBehavior.ESTIMATE);
                        Calendar cal1 = Calendar.getInstance();
                        Calendar cal2 = Calendar.getInstance();
                        cal1.setTime(serverDate);
                        cal2.setTime(lastReportDate);
                        boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);



                        if(!sameDay)
                        {
                            reportCountRef.update("reportCount", 1, "timestamp", FieldValue.serverTimestamp())
                            .addOnCompleteListener(completeListener);
                        }
                        else
                        {
//                            current_today_report_count = lastReportDocumentSnapshot.get("reportCount", Integer.class) + 1;

                            Map<String, Object> data = new HashMap<>();
                            data.put("reportCount", FieldValue.increment(1));
                            data.put("timestamp", FieldValue.serverTimestamp());

//                            int finalCurrent_today_report_count = current_today_report_count;

                            reportCountRef.set(data, SetOptions.merge())
                            .addOnCompleteListener(completeListener);

                        }
                    }
                    else
                    {
                        Map<String, Object> data = new HashMap<>();
                        data.put("reportCount", FieldValue.increment(1));
                        data.put("timestamp", FieldValue.serverTimestamp());
                        reportCountRef.set(data, SetOptions.merge())
                                .addOnCompleteListener(completeListener);
                    }

//                        DocumentReference reportCountRef = db.collection("TodaysReport").document("current");



                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressBar(false);
                        showToast(e.getMessage());
                    }
                });
            }
        });

    }


    private boolean hasSameCategory(List<String> cat1, List<String> cat2)
    {
        Set<String> set = new HashSet<>(cat2);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Arrays.stream(cat1.toArray()).anyMatch(set::contains);
        }
        else
        {
            return Collections.disjoint(
                    Arrays.asList(cat1), Arrays.asList(cat2));
        }
    }

    static class CategoryDocument {
        public List<Category> categories;

        public CategoryDocument() {}

        public List<Category> getCategories() {
            return categories;
        }

        public void setCategories(List<Category> categories) {
            this.categories = categories;
        }
    }

    private void processIfThisReportIsOKForCombination(QuerySnapshot queryDocumentSnapshots, List<Category> realSelectedCategories, String suspect_first_name, String suspect_last_name, String content, String gender, List<String> selectedCategories, List<String> mediaPaths, List<String> selectedDescriptions)
    {
        boolean thisReportIsOK = false;
        if(!suspect_first_name.isEmpty()) {
            JoinReport(queryDocumentSnapshots.getDocuments().get(0).getId(), (String) queryDocumentSnapshots.getDocuments().get(0).get("reportId"), content, (ArrayList<String>) queryDocumentSnapshots.getDocuments().get(0).get("senders"), realSelectedCategories, selectedDescriptions, (ArrayList<String>) mediaPaths);
            return;
        }


        for(int z = 0; z < queryDocumentSnapshots.getDocuments().size(); z++)
        {
            DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(z);

            ArrayList<String> thisReportDescriptions = (ArrayList<String>) documentSnapshot.get("descriptions");


            // ^ we need to check if this report description doesn't contains an opposite description
            for(int i=0; i < selectedDescriptions.size(); i++)
            {
                boolean hasOpposite = personDescriptions.hasOpposite(selectedDescriptions.get(i), thisReportDescriptions);
                thisReportIsOK = !hasOpposite;

                if(thisReportIsOK == false && z < queryDocumentSnapshots.getDocuments().size()-1)
                    continue;
            }

            // and finally, if this report at least has the same category...
            List<Category> thisReportCategories = documentSnapshot.toObject(CategoryDocument.class).categories;
            List<String> thisReportCategoryNames = new ArrayList<>();
            for (Category category : thisReportCategories)
            {
                thisReportCategoryNames.add(category.getName());
            }

//            thisReportIsOK = hasSameCategory(selectedCategories, thisReportCategoryNames);

            if(thisReportIsOK)
            {
                JoinReport(documentSnapshot.getId(), (String)documentSnapshot.get("reportId"), content, (ArrayList<String>)documentSnapshot.get("senders"), realSelectedCategories, selectedDescriptions, (ArrayList<String>) mediaPaths);
                break;
            }
            else {
                // No report that matches
                SendNewReport(suspect_first_name, suspect_last_name, content, gender, realSelectedCategories, mediaPaths, selectedDescriptions);
                break;
            }
        }
    }

    private void SendReport(String suspect_first_name, String suspect_last_name, String content, String gender, List<String> selectedCategories, List<String> mediaPaths, List<String> selectedDescriptions)
    {
        progressBar(true);

        // Default query by description


        List<Category> realSelectedCategories = new ArrayList<>();

        for (String category_name : selectedCategories)
        {
            realSelectedCategories.add(cached_categories.get(category_name));
        }

        if(!suspect_first_name.isEmpty() && !suspect_last_name.isEmpty()) {
            SendNewReport(suspect_first_name, suspect_last_name, content, gender, realSelectedCategories, mediaPaths, selectedDescriptions);
            //query = db.collection("reports").whereEqualTo("gender", gender).whereEqualTo("suspect", suspect_first_name.toLowerCase()).whereEqualTo("suspect_surname", suspect_last_name.toLowerCase()).whereGreaterThan("status", 0);
        }
        else {
            List<String> first10Descriptions = new ArrayList();
            List<String> remainderDescriptions = new ArrayList<>();

            if(selectedDescriptions.size() > 0)
            {
                for (int i=0; i < selectedDescriptions.size(); i++)
                {
                    if(i > 9)
                    {
                        remainderDescriptions.add(selectedDescriptions.get(i));
                    }
                    else
                    {
                        first10Descriptions.add(selectedDescriptions.get(i));
                    }
                }

                Query query = db.collection("reports").whereEqualTo("gender", gender).whereArrayContainsAny("descriptions", first10Descriptions).whereGreaterThan("status", 0);

                query.get().addOnSuccessListener(queryDocumentSnapshots -> {

                    // There's already an existing report
                    if (queryDocumentSnapshots.getDocuments().size() > 0) {
                        processIfThisReportIsOKForCombination(queryDocumentSnapshots, realSelectedCategories, suspect_first_name, suspect_last_name, content, gender, selectedCategories, mediaPaths, selectedDescriptions);
                    } else {
                        //second process
                        if (remainderDescriptions.size() > 0) {
                            Query secondDescriptionQuery = db.collection("reports").whereEqualTo("gender", gender).whereArrayContainsAny("descriptions", remainderDescriptions).whereGreaterThan("status", 0);
                            secondDescriptionQuery.get().addOnCompleteListener(task -> {
                                if (task.isSuccessful()) {
                                    if (task.getResult().getDocuments().size() > 0) {
                                        processIfThisReportIsOKForCombination(task.getResult(), realSelectedCategories, suspect_first_name, suspect_last_name, content, gender, selectedCategories, mediaPaths, selectedDescriptions);
                                    } else {
                                        SendNewReport(suspect_first_name, suspect_last_name, content, gender, realSelectedCategories, mediaPaths, selectedDescriptions);
                                    }
                                } else {
                                    SendNewReport(suspect_first_name, suspect_last_name, content, gender, realSelectedCategories, mediaPaths, selectedDescriptions);
                                }
                            });
                        } else {
                            SendNewReport(suspect_first_name, suspect_last_name, content, gender, realSelectedCategories, mediaPaths, selectedDescriptions);
                        }
//                return;
                    }

                }).addOnFailureListener(e -> {
                    progressBar(false);
                    Log.d(TAG, e.getMessage());
                    showToast(e.getMessage());
                });
            }





        }
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Integer.parseInt(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private List<String> StringToList(String str, String separator)
    {
        String[] splitted = str.split(separator);
        List<String> returnList = new ArrayList<>();//new ArrayList<String>(report.child("categories").getValue(String.class).split(","));
        for (int i=0; i < splitted.length; i++)
        {
            returnList.add(splitted[i]);
        }

        return  returnList;
    }



    private String ListToString(List<String> list, String separator)
    {
        String result = "";
        for (int i=0; i < list.size(); i++)
        {
            if(i == 0)
                result = list.get(0);
            else
                result += separator  + list.get(i);
        }

        return result;
    }


    private void progressBar(boolean show) {
        View support_layout = findViewById(R.id.support_layout);

        if (show == false) {
            support_layout.setVisibility(View.GONE);
        } else {
            support_layout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void preventClicks(View view) {}
}
