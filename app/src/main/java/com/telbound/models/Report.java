package com.telbound.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Report {
    public String reportId, postedAt, suspect, suspect_surname, gender, key;
    private int status;
    private List<Category> categories;// color, name
    private List<String> senders;
    private List<String> descriptions;
    private List<String> resolve_seminars;
    private String resolve_date, resolve_description;


    public Report()
    {}

    public Report(String reportId, String postedAt, List<Category> categories, List<String> descriptions, List<String> senders, String suspect, String suspect_surname, String gender, int status) {
        this.reportId = reportId;
//        this.thread = thread;
        this.postedAt = postedAt;
        this.categories = categories;
        this.descriptions = descriptions;
        this.senders = senders;
        this.suspect = suspect;
        this.suspect_surname = suspect_surname;
        this.gender = gender;
        this.status = status;
    }



    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

//    public String getThread() {
//        return thread;
//    }
//
//    public void setThread(String thread) {
//        this.thread = thread;
//    }

    public String getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(String postedAt) {
        this.postedAt = postedAt;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<String> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(ArrayList<String> descriptions) {
        this.descriptions = descriptions;
    }

    public List<String> getSenders() {
        return senders;
    }

    public void setSenders(List<String> senders) {
        this.senders = senders;
    }

    public String getSuspect() {
        return suspect;
    }

    public void setSuspect(String suspect) {
        this.suspect = suspect;
    }

    public String getSuspect_surname() {
        return suspect_surname;
    }

    public void setSuspect_surname(String suspect_surname) {
        this.suspect_surname = suspect_surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<String> getResolve_seminars() {
        return resolve_seminars;
    }

    public String getResolve_date() {
        return resolve_date;
    }

    public String getResolve_description() {
        return resolve_description;
    }
}
