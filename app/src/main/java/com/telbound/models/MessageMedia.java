package com.telbound.models;

import android.net.Uri;

public class MessageMedia extends Message {
    private Uri uri;
    private String filename, mimetype;

    public MessageMedia(Uri uri, String filename, String mimetype) {
        this.uri = uri;
        this.filename = filename;
        this.mimetype = mimetype;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }
}
