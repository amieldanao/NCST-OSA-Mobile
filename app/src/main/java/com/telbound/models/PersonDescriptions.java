package com.telbound.models;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PersonDescriptions {
    private static final String TAG = "myLogTag";
    public HashMap<String, List<String>> descriptions;

    public PersonDescriptions() {
        descriptions = new HashMap<>();
//        descriptions.put("large eyes", new String[]{"small eyes", "sharp eyes", "squinty eyes", "round eyes"});
//        descriptions.put("small eyes", new String[]{"large eyes", "sharp eyes", "squinty eyes", "round eyes"});
//        descriptions.put("sharp eyes", new String[]{"large eyes", "small eyes", "squinty eyes", "round eyes"});
//        descriptions.put("squinty eyes", new String[]{"large eyes", "small eyes", "sharp eyes", "round eyes"});
//        descriptions.put("round eyes", new String[]{"large eyes", "small eyes", "sharp eyes", "squinty eyes"});

    }

    public boolean hasOpposite(String toCheck, ArrayList<String> description_present)
    {
        if(descriptions.containsKey(toCheck))
        {
//            String converted_list_description = description_present.toString();
            Log.d(TAG, toCheck);
            Log.d(TAG, description_present.toString());
            List<String> opposites = descriptions.get(toCheck);
            for(int i=0; i < opposites.size(); i++) {
                String thisOpposite = opposites.get(i);
                Log.d(TAG, toCheck + " - has opposite : " + thisOpposite);
                if (description_present.contains(thisOpposite))
                {
                    return true;
                }
            }
        }

        return false;
    }
}
