package com.telbound.models;

import org.json.JSONArray;

public class Thread {

    private JSONArray listUri;
    private String content;
    private boolean isWeb;

    public Thread(JSONArray listUri, String content, boolean isWeb) {
        this.listUri = new JSONArray();

        this.listUri = listUri;
        this.content = content;
        this.isWeb = isWeb;
    }

    public JSONArray getListUri() {
        return listUri;
    }

    public void setListUri(JSONArray listUri) {
        this.listUri = listUri;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean getIsWeb() {
        return isWeb;
    }

    public void setIsWeb(boolean isWeb) {
        this.isWeb = isWeb;
    }
}
