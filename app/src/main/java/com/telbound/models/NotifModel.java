package com.telbound.models;

import java.util.Date;
import java.util.List;

public class NotifModel {
    private String message, title, reportId;
    private Date dateSent;
    private boolean resolved;
    private List<String> recipients;
    private List<String> seenedBy;
    private String type;
    public String key;

    public NotifModel(String message, String title, Date dateSent) {
        this.message = message;
        this.title = title;
        this.dateSent = dateSent;
    }

    public NotifModel() {
    }

    public List<String> getSeenedBy() {
        return seenedBy;
    }

    public void setSeenedBy(List<String> seenedBy) {
        this.seenedBy = seenedBy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public boolean isResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
