package com.telbound.models;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Message implements Comparable<Message>{
    String message, user, file_name, user_id, key, reportId;

    @ServerTimestamp
    private Date date;

//            file_name;
    boolean is_media, is_admin;
    boolean initial = false;

    public Message(){}

    public Message(String message, String user, String user_id, boolean is_media, String file_name, boolean is_admin) {
        this.message = message;
        this.user = user;
        this.user_id = user_id;
        this.is_media = is_media;
        this.file_name = file_name;
        this.is_admin = is_admin;
    }

    public boolean isInitial() {
        return initial;
    }

    public void setInitial(boolean initial) {
        this.initial = initial;
    }

    @Override
    public int compareTo(Message o) {
        return getDate().compareTo(o.getDate());
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

//    public String getMsg_date() {
//        return msg_date;
//    }
//
//    public void setMsg_date(String msg_date) {
//        this.msg_date = msg_date;
//    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
//
//    public String getKey() {
//        return key;
//    }
//
//    public void setKey(String key) {
//        this.key = key;
//    }

    public boolean isIs_media() {
        return is_media;
    }

    public void setIs_media(boolean is_media) {
        this.is_media = is_media;
    }

    public boolean isIs_admin() {
        return is_admin;
    }

    public void setIs_admin(boolean is_admin) {
        this.is_admin = is_admin;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
