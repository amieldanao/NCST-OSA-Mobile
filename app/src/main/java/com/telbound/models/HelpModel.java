package com.telbound.models;

public class HelpModel {
    private String section, content;
    public String key;

    public HelpModel(){}

    public HelpModel(String section, String content) {
        this.section = section;
        this.content = content;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
