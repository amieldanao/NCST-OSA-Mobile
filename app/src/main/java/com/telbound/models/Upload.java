package com.telbound.models;

import com.google.firebase.storage.UploadTask;

public class Upload {
    private String filename, origFilenameOnly;
    private UploadTask uploadTask;
    private long fileSize;

    public Upload(String filename, String origFilenameOnly, UploadTask uploadTask, long fileSize) {
        this.filename = filename;
        this.origFilenameOnly = origFilenameOnly;
        this.uploadTask = uploadTask;
        this.fileSize = fileSize;
    }

    public String getOrigFilenameOnly() {
        return origFilenameOnly;
    }

    public void setOrigFilenameOnly(String origFilenameOnly) {
        this.origFilenameOnly = origFilenameOnly;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public UploadTask getUploadTask() {
        return uploadTask;
    }

    public void setUploadTask(UploadTask uploadTask) {
        this.uploadTask = uploadTask;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
}
