package com.telbound.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.telbound.fragments.main.MediaView;
import com.telbound.models.Message;
import com.telbound.models.MessageMedia;
import com.telbound.orca.Chat;
import com.telbound.orca.MainActivity;
import com.telbound.orca.R;
import com.telbound.utils.AsyncResponse;
import com.telbound.utils.ImageCache;
import com.telbound.utils.MyURLChecker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//public class MessageAdapater extends ArrayAdapter<Message>
public class MessageAdapater extends RecyclerView.Adapter<MessageAdapater.MessageViewHolder> implements AsyncResponse {
    private static final String TAG = "myLogTag";

    private List<Message> messageList;
    private Context context;

    private Chat chat;
    public ImageCache imageCache;
    private RequestOptions requestOptions;
    private final int round_radius = 32;
    private final int image_chat_padding_side = 270;
    private HashMap<String, MessageMedia> messageMediaHashMap;
    private View parent_view;

    private HashMap<String, String> valid_urls_cache;
    private HashMap<String, String> valid_video_urls_cache;
    private HashMap<String, String> valid_image_urls_cache;


//    private MessageAdapater.MessageViewHolder viewHolder;

    public MessageAdapater(Context context, Chat c) {
        this.chat = c;
        this.messageList = chat.messageList;
        this.imageCache = new ImageCache();
        this.requestOptions = new RequestOptions();
        this.requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(round_radius));
        this.messageMediaHashMap = new HashMap<>();
        this.valid_urls_cache = new HashMap<>();
        this.valid_video_urls_cache = new HashMap<>();
        this.valid_image_urls_cache = new HashMap<>();
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tv_msg_date;
        public ImageView chat_sender_pic;
//        public LinearLayout message_container;
        public TextView tv_main_message;
        public ImageView img_chat_message;
        public ImageView img_video_play;
//        public ConstraintLayout img_parent;
        public ConstraintLayout main_constraint;
//        public TextView sender_name;
        public CardView card_message;
        public CardView card_image;
//        public ViewGroup.LayoutParams origParams;
        public WebView initialMessageView;
        public ConstraintLayout.LayoutParams initialParams;

        public MessageViewHolder(View parent) {
            super(parent);
            this.tv_msg_date = parent.findViewById(R.id.txt_message_date);
            this.chat_sender_pic = parent.findViewById(R.id.message_sender_pic);
//            this.message_container = parent.findViewById(R.id.message_container);
            this.tv_main_message = parent.findViewById(R.id.txt_chat_message);
            this.img_chat_message = parent.findViewById(R.id.img_chat_mesage);
            this.img_video_play = parent.findViewById(R.id.play_icon);
//            this.img_parent = parent.findViewById(R.id.img_parent);
//            this.sender_name = parent.findViewById(R.id.txt_sender_name);
            this.card_message = parent.findViewById(R.id.msg_card);


            this.card_image = parent.findViewById(R.id.img_card);
            this.main_constraint = parent.findViewById(R.id.main_constraint);
            this.initialMessageView = parent.findViewById(R.id.initialMessageView);
//            this.origParams = initialMessageView.getLayoutParams();

            this.initialParams = (ConstraintLayout.LayoutParams) this.initialMessageView.getLayoutParams();
            this.initialParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
//            initialParams.constrainedWidth = true;
//            initialParams.matchConstraintMaxWidth = ConstraintLayout.LayoutParams.MATCH_CONSTRAINT_WRAP;
//            initialParams.matchConstraintDefaultWidth = ConstraintLayout.LayoutParams.MATCH_CONSTRAINT_PERCENT;
//            initialParams.matchConstraintPercentWidth = 1f;
//            initialMessageView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ONLY);
//            initialMessageView.getSettings().setUseWideViewPort(false);

        }

        private void showVideoEntry()
        {
            this.card_message.setVisibility(View.GONE);
            this.card_image.setVisibility(View.VISIBLE);
            this.img_video_play.setVisibility(View.VISIBLE);
        }

        private void showImageEntry()
        {
            this.card_message.setVisibility(View.GONE);
            this.card_image.setVisibility(View.VISIBLE);
            this.img_video_play.setVisibility(View.GONE);
        }

        private void showTextEntry()
        {
            this.card_image.setVisibility(View.GONE);
            this.img_video_play.setVisibility(View.GONE);
            this.card_message.setVisibility(View.VISIBLE);
        }

        private void toggleSenderName(boolean on)
        {
//            this.sender_name.setVisibility(on? View.VISIBLE : View.GONE );
        }

        private void toggleSenderPic(boolean on)
        {
            this.chat_sender_pic.setVisibility(on? View.VISIBLE : View.GONE );
        }
    }



    @Override
    public MessageAdapater.MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View v  = inflater.inflate(R.layout.chat_message_entry, parent, false);

        // Return a new holder instance
//        return new MessageAdapater.MessageViewHolder(parent_view);
        return new MessageAdapater.MessageViewHolder(v);
//        viewHolder = new MessageAdapater.MessageViewHolder(parent_view);
//        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {

        Message thisMessage = messageList.get(position);


        if(thisMessage.getDate() != null) {

            holder.tv_msg_date.setText(thisMessage.getDate().toString().split("GMT")[0]);

            holder.tv_msg_date.setVisibility(View.VISIBLE);
        }

        //DEFAULTS
        holder.tv_main_message.setText(R.string.blank);
        holder.img_video_play.setVisibility(View.GONE);

        if(thisMessage.isInitial())
        {

//            holder.initialMessageView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
//            holder.initialMessageView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
//            holder.initialMessageView.setLayoutParams(holder.initialParams);


            holder.card_message.setVisibility(View.GONE);
            holder.card_image.setVisibility(View.GONE);
            holder.img_video_play.setVisibility(View.GONE);
            holder.initialMessageView.setVisibility(View.VISIBLE);
//            holder.main_constraint.setLayoutDirection(View.LAYOUT_DIRECTION_INHERIT);
//            setInitialText(holder, thisMessage);
            holder.chat_sender_pic.setVisibility(View.GONE);
        }
        else
        {
//            holder.initialMessageView.setLayoutParams(holder.origParams);
            holder.initialMessageView.setVisibility(View.GONE);
        }

        int type = 0;

        //add chat head if we're not the sender
        if(!chat.UserEmail.equals(thisMessage.getUser()))
        {
            type = 1;
//            if(!thisMessage.isInitial())
                holder.main_constraint.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);


            if(thisMessage.isIs_admin())
                holder.toggleSenderPic(true);
            else
                holder.toggleSenderPic(false);

            holder.card_message.setCardBackgroundColor(Color.parseColor("#FFF59D"));
        }
        else
        {
//            if(!thisMessage.isInitial())
                holder.main_constraint.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.toggleSenderPic(false);
            holder.card_message.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
        }





        if(thisMessage.isIs_media())
            holder.showImageEntry();
        else if(thisMessage.isInitial() == false)
            holder.showTextEntry();

        if(thisMessage.isIs_media()) {
            //check if this file is already in cache
            Bitmap tryBitmap = imageCache.getBitmapFromMemCache(thisMessage.getFile_name());
            if(tryBitmap == null) {
                //try gif
                Uri tryUri = imageCache.getGifUriFromCache(thisMessage.getFile_name());
                if(tryUri == null) {

                    loadMessageImage(holder.img_chat_message, thisMessage.getFile_name(), holder);
                }
                else {
                    loadGIF(holder.img_chat_message, tryUri, thisMessage.getFile_name());
                    Log.d(TAG, "gif loaded from cache " + thisMessage.getFile_name());
                }
            }
            else {
                holder.img_chat_message.setImageBitmap(tryBitmap);


                MessageMedia tryMessageMedia = messageMediaHashMap.get(thisMessage.getFile_name());
                if(tryMessageMedia != null) {

                    holder.img_chat_message.setTag(tryMessageMedia);
                    holder.img_chat_message.setClickable(true);
                    if(!holder.img_chat_message.hasOnClickListeners()) {
                        holder.img_chat_message.setOnClickListener(clickListener);
                    }
                    //video play icon on thumbnail
                    if(tryMessageMedia.getMimetype().contains("video"))
                    {
//                        holder.img_video_play.setVisibility(View.VISIBLE);
                        holder.showVideoEntry();
                    }
                    else
                    {
                        holder.showImageEntry();
                    }


                    Log.d(TAG, "added click listener manually : " + thisMessage.getFile_name());

                }

                Log.d(TAG, "image loaded from cache " + thisMessage.getFile_name());
            }
        }
        else
        {
            //we will check if this media link message is already viewed earlier and is on cache
            Bitmap tryBitmap = imageCache.image_links_cache.get(thisMessage.getKey());

            if(valid_urls_cache.containsKey(thisMessage.getKey()))//check if generic link is cached
            {

//                holder.tv_main_message.setVisibility(View.VISIBLE);
//                holder.card_message.setVisibility(View.VISIBLE);
//                holder.img_chat_message.setVisibility(View.GONE);
                holder.tv_main_message.setText(valid_urls_cache.get(thisMessage.getKey()));
                holder.tv_main_message.setMovementMethod(LinkMovementMethod.getInstance());
                if(thisMessage.isInitial() == false)
                    holder.showTextEntry();
                return;
            }
            else if(tryBitmap != null)
            {
//                holder.tv_main_message.setVisibility(View.GONE);
//                holder.card_message.setVisibility(View.GONE);
//                holder.img_chat_message.setVisibility(View.VISIBLE);


                MessageMedia newMessageMedia = messageMediaHashMap.get(thisMessage.getKey());
                if(newMessageMedia.getMimetype().equals("video"))
                    holder.showVideoEntry();
                else
                    holder.showImageEntry();
                holder.img_chat_message.setImageBitmap(tryBitmap);
                holder.img_chat_message.setTag(newMessageMedia);
                attachClickListener(holder.img_chat_message);
                return;
            }
            else {
                Uri tryUri = imageCache.getGifUriFromCache(thisMessage.getKey());

                //check if message is url
                if (tryUri == null && URLUtil.isValidUrl(thisMessage.getMessage()) && !thisMessage.getMessage().trim().contains(" ")) {
//                    MyURLChecker asyncTask = new MyURLChecker(thisMessage.getMessage(), thisMessage.key, thisMessage, holder.tv_main_message, holder.card_message, holder.img_chat_message, holder.img_video_play);
                    MyURLChecker asyncTask = new MyURLChecker(thisMessage.getMessage(), thisMessage.getKey(), thisMessage, holder);



                    asyncTask.delegate = this;
                    asyncTask.execute();
                    return;

                }
                else {

//                    if(thisMessage.getMessage().contains(".gif"))
//                        Log.d(TAG, "gif");

                    if(messageMediaHashMap.containsKey(thisMessage.getKey())) {
                        MessageMedia newMessageMedia = messageMediaHashMap.get(thisMessage.getKey());
                        if(newMessageMedia.getMimetype().equals("video"))
                            holder.showVideoEntry();
                        else if(newMessageMedia.getMimetype().contains("gif")) {

                            if(tryUri == null) {

                                if(thisMessage.isInitial())
                                    setInitialText(holder, thisMessage);
                                else
                                    holder.tv_main_message.setText(thisMessage.getMessage());
//                                holder.tv_main_message.setText(thisMessage.getMessage());
                                if(thisMessage.isInitial() == false)
                                    holder.showTextEntry();
//                                loadMessageImage(holder.img_chat_message, thisMessage.getFile_name(), holder);
                            }
                            else {
                                loadGIFFromLink(thisMessage.getKey(), tryUri.toString(), holder);
//                                loadGIF(holder.img_chat_message, tryUri, thisMessage.getKey());
                            }
                        }
                        else
                            holder.showImageEntry();
                    }
                    else
                    {
                        if(thisMessage.isInitial())
                            setInitialText(holder, thisMessage);
                        else
                            holder.tv_main_message.setText(thisMessage.getMessage());

                        if(thisMessage.isInitial() == false)
                            holder.showTextEntry();
                    }

                    return;
//                    holder.tv_main_message.setVisibility(View.VISIBLE);
//                    holder.card_message.setVisibility(View.VISIBLE);
//                    holder.img_chat_message.setVisibility(View.GONE);
                }
            }
        }

//        holder.message_container.setLayoutParams(lp2);
    }

    private void setInitialText(MessageViewHolder holder, Message message) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            holder.tv_main_message.setText(Html.fromHtml(message.getMessage(), Html.FROM_HTML_OPTION_USE_CSS_COLORS));
//        } else {
//            holder.tv_main_message.setText(Html.fromHtml(message.getMessage()));
//        }

        holder.card_message.setVisibility(View.GONE);
        holder.card_image.setVisibility(View.GONE);
        holder.img_video_play.setVisibility(View.GONE);
        holder.chat_sender_pic.setVisibility(View.GONE);
        holder.initialMessageView.setVisibility(View.VISIBLE);
        holder.initialMessageView.loadDataWithBaseURL(null, message.getMessage(), "text/html; charset=utf-8", "UTF-8", null);
        holder.initialMessageView.setLayoutParams(holder.initialParams);
        //            holder.main_constraint.setLayoutDirection(View.LAYOUT_DIRECTION_INHERIT);
//            setInitialText(holder, thisMessage);

    }


    //    public void processFinish(String output, String key, Message message, TextView txt_view, CardView myCardView, ImageView imgView, ImageView videoIcon) {
    @Override
    public void processFinish(String output, String key, Message message, MessageViewHolder myMessageViewHolder) {
        Log.d(TAG, "Done asyncTask " + output);

        String link = message.getMessage();
        if(output == null)
            return;

        if(output.contains("text/html") || output.contains("video")) {
            boolean isVideo = link.contains("www.youtube.com") || link.contains("https://youtu.be/") || output.contains("video");
            //check if this is a youtube video
            if(isVideo)
            {
                String videoLink;

                if(link.contains("www.youtube.com") || link.contains("https://youtu.be/")) {
                    videoLink = convertYoutubeLinkToThumbnailLink(link);

                    Log.d(TAG, videoLink);

                    SimpleTarget target = new SimpleTarget<Bitmap>() {

                        @Override
                        public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {

                            myMessageViewHolder.img_chat_message.setImageBitmap(bitmap);
                            myMessageViewHolder.showVideoEntry();
//                            imgView.setVisibility(View.VISIBLE);
//                            txt_view.setVisibility(View.GONE);
//                            myCardView.setVisibility(View.GONE);
                            imageCache.image_links_cache.put(key, bitmap);

                            MessageMedia newMessageMedia = new MessageMedia(Uri.parse(link), key, "video");
                            messageMediaHashMap.put(key, newMessageMedia);

//                            videoIcon.setVisibility(View.VISIBLE);

                            myMessageViewHolder.img_chat_message.setTag(newMessageMedia);
                            attachClickListener(myMessageViewHolder.img_chat_message);

                            Log.d(TAG, "added video link thumbnail to cache");
                        }
                    };

                    if(ImageCache.isValidContextForGlide(context)) {
                        Glide.with(context).asBitmap().load(videoLink).placeholder(R.drawable.ic_photo_black_24dp)
                                .error(R.drawable.ic_photo_black_24dp)
                                .apply(requestOptions)
                                .into(target);
                    }
                }
                else
                {
                    if (!valid_urls_cache.containsKey(key))
                        valid_urls_cache.put(key, link);
//                    txt_view.setText(Html.fromHtml(link, new GlideImageGetter(txt_view, false, false, null), null));
                    myMessageViewHolder.tv_main_message.setText(link);
                    myMessageViewHolder.tv_main_message.setMovementMethod(LinkMovementMethod.getInstance());

                    if(message.isInitial() == false)
                        myMessageViewHolder.showTextEntry();

                    Log.e(TAG, output + " : 1"  + message.getMessage());
                }

            }
            else {

                if(link.endsWith(".gif"))
                {
                    Log.e(TAG, output + " : 2"  + message.getMessage());
                    loadGIFFromLink(key, link, myMessageViewHolder);
                }
                else {
                    if (!valid_urls_cache.containsKey(key))
                        valid_urls_cache.put(key, link);
//                txt_view.setText(Html.fromHtml(link, new GlideImageGetter(txt_view, false, false, null), null));

//                    Log.e(TAG, output + " : 2" + message.getMessage());

                    myMessageViewHolder.tv_main_message.setText(link);
                    myMessageViewHolder.tv_main_message.setMovementMethod(LinkMovementMethod.getInstance());

                    if(message.isInitial() == false)
                        myMessageViewHolder.showTextEntry();
                }
//                myCardView.setVisibility(View.VISIBLE);
//                txt_view.setVisibility(View.VISIBLE);
//                imgView.setVisibility(View.GONE);
//                videoIcon.setVisibility(View.GONE);
            }
        }
        else if(output.contains("image"))
        {
            SimpleTarget target = new SimpleTarget<Bitmap>() {

                @Override
                public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
//                Drawable dr = new BitmapDrawable(chat.getResources(), bitmap);
                    myMessageViewHolder.img_chat_message.setImageBitmap(bitmap);
                    imageCache.image_links_cache.put(key, bitmap);

                    String mimeType = "image/jpeg";
                    String[] splitOutput = output.split(";");
                    if(splitOutput.length > 1)
                        mimeType = splitOutput[0];

                    MessageMedia newMessageMedia = new MessageMedia(Uri.parse(link), key, mimeType);
                    messageMediaHashMap.put(key, newMessageMedia);

//                    videoIcon.setVisibility(View.GONE);
//                    txt_view.setVisibility(View.GONE);
//                    myCardView.setVisibility(View.GONE);

                    myMessageViewHolder.showImageEntry();
                    myMessageViewHolder.img_chat_message.setTag(newMessageMedia);
//                    imgView.setVisibility(View.VISIBLE);
                    attachClickListener(myMessageViewHolder.img_chat_message);
                }
            };

//            imageCache.addBitmapToUriCache(filename, uri);

//            Log.e(TAG, output + " : 3"  + message.getMessage());
            if(output.contains("gif"))
            {
                loadGIFFromLink(key, link, myMessageViewHolder);
                Log.e(TAG, "THIS IS GIF!!!!!!!!!!!!!!!   " + link);
            }
            else
            {
                if(ImageCache.isValidContextForGlide(context)) {
                    Glide.with(context).asBitmap().load(link).placeholder(R.drawable.ic_photo_black_24dp)
                            .error(R.drawable.ic_photo_black_24dp)
                            .apply(requestOptions)
                            .into(target);
                }
            }
        }
    }

    private void loadGIFFromLink(String key, String link, MessageViewHolder myMessageViewHolder)
    {
        if(ImageCache.isValidContextForGlide(context)) {
            Glide
                    .with(context)
                    .asGif()
                    .load(Uri.parse(link))
                    .listener(new RequestListener<GifDrawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
                            Log.e(TAG, e.getMessage() + " >>> " + link);
                            myMessageViewHolder.tv_main_message.setText(link);
                            myMessageViewHolder.tv_main_message.setMovementMethod(LinkMovementMethod.getInstance());


                            myMessageViewHolder.showTextEntry();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {

                            if (imageCache.getGifUriFromCache(key) == null)
                                imageCache.addGifUriToCache(key, Uri.parse(link));

                            MessageMedia newMessageMedia = new MessageMedia(Uri.parse(link), key, "image/gif");
                            if (!messageMediaHashMap.containsKey(key)) {
                                messageMediaHashMap.put(key, newMessageMedia);
                            }

                            myMessageViewHolder.img_chat_message.setTag(newMessageMedia);
                            attachClickListener(myMessageViewHolder.img_chat_message);

                            Log.d(TAG, "LOADED GIF " + link);
                            myMessageViewHolder.showImageEntry();
                            return false;
                        }
                    })
                    .error(R.drawable.ic_photo_black_24dp)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(myMessageViewHolder.img_chat_message);

            myMessageViewHolder.showImageEntry();
        }

    }

    private String convertYoutubeLinkToThumbnailLink(String ytlink)
    {
        String videoId;
        if(ytlink.contains("https://youtu.be"))
            videoId = ytlink.split("https://youtu.be/")[1];
        else
            videoId = ytlink.split("v=")[1];
        return "http://img.youtube.com/vi/" + videoId + "/maxresdefault.jpg";

//        "http://i3.ytimg.com/vi/32si5cfrCNc/maxresdefault.jpg"
//        ret = "https://i.ytimg.com/vi/" +ytlink.split("v=")[1] + "/maxresdefault.jpg";
    }

    private void attachClickListener(View btn)
    {
        btn.setClickable(true);
//        if(!btn.hasOnClickListeners())
        btn.setOnClickListener(clickListener);
    }

    final View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            MediaView.newInstance(Uri.parse(v.getTag(0).toString()), v.getTag(1).toString(), v.getTag(2).toString());
            Log.d(TAG, "you clicked an image!");
//            chat.getSupportActionBar().hide();
            MessageMedia messageMedia = (MessageMedia) v.getTag();
            if(messageMedia.getUri().toString().contains("www.youtube.com") || messageMedia.getUri().toString().contains("https://youtu.be/"))
            {
                Chat.instance.startActivity(new Intent(Intent.ACTION_VIEW, messageMedia.getUri()));
            }else {
                chat.getSupportFragmentManager().beginTransaction().add(R.id.frameContainer, MediaView.newInstance(messageMedia)).addToBackStack("mediaView").commit();
            }
        }
    };

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return messageList.size();
    }

//    private void isMessageImage(boolean image, MessageViewHolder holder)
//    {
//        if(image)
//        {
//            holder.tv_main_message.setVisibility(View.GONE);
//            holder.card_message.setVisibility(View.GONE);
//            holder.img_chat_message.setVisibility(View.VISIBLE);
//        }
//        else
//        {
//            holder.tv_main_message.setVisibility(View.VISIBLE);
//            holder.card_message.setVisibility(View.VISIBLE);
//            holder.img_chat_message.setVisibility(View.GONE);
//        }
//    }

    private void loadMessageImage(ImageView imgView, String filename, MessageViewHolder myMessageViewHolder)
    {
        //check mime type
        // Create a storage reference from our app
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();

        // Get reference to the file
        StorageReference imageRef = storageRef.child("reports/" + chat.report_id + "/" + filename);

        imageRef.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
            @Override
            public void onSuccess(StorageMetadata storageMetadata) {
                // Metadata now contains the metadata for 'images/forest.jpg'
                String mimeType = storageMetadata.getContentType();

                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {

                        if(mimeType.contains("video"))
                        {
//                            video_play_icon.setVisibility(View.VISIBLE);
                            loadVideo(imgView, uri, filename, myMessageViewHolder);
                        }
                        else
                        {
                            switch (mimeType) {
                                case "image/jpeg" : loadJPG(imgView, uri, filename);break;
                                case "image/gif" : loadGIF(imgView, uri, filename); break;
                            }
                        }
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Uh-oh, an error occurred!
            }
        });
    }

    private void loadVideo(ImageView imgView, Uri theVideoUri, String filename, MessageViewHolder myMessageViewHolder)
    {

        String videoThumbnailFileName = filename.split("\\.")[0] + ".jpg";
        StorageReference reference = FirebaseStorage.getInstance().getReference()
                .child("video_thumbnails/" + chat.report_id + "/" + videoThumbnailFileName);


        reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                SimpleTarget target = new SimpleTarget<Bitmap>() {

                    @Override
                    public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
//                Drawable dr = new BitmapDrawable(chat.getResources(), bitmap);
                        imgView.setImageBitmap(bitmap);
                        imageCache.addBitmapToMemoryCache(filename, bitmap);
                        imageCache.addBitmapToUriCache(filename, uri);
                        Log.d(TAG, "added video thumbnail to cache");

                    }
                };

                myMessageViewHolder.showVideoEntry();
//                videoIcon.setVisibility(View.VISIBLE);

                if(ImageCache.isValidContextForGlide(context)) {
                    Glide.with(context).asBitmap().load(uri).placeholder(R.drawable.ic_ondemand_video_black_24dp)
                            .error(R.drawable.ic_ondemand_video_black_24dp)
                            .apply(requestOptions)
                            .into(target);
                }

                MessageMedia newMessageMedia = new MessageMedia(theVideoUri, filename, "video");
                messageMediaHashMap.put(filename, newMessageMedia);

                imgView.setTag(newMessageMedia);
                attachClickListener(imgView);
            }
        });



//        Drawable dr = chat.getResources().getDrawable(R.drawable.ic_ondemand_video_black_24dp);
//        imgView.setImageDrawable(dr);

    }

    private void loadGIF(ImageView imgView, Uri uri, String filename)
    {
        Glide
                .with(Chat.context)
                .asGif()
                .load(uri)
                .error(R.drawable.ic_photo_black_24dp)
                .apply(requestOptions)
                .into(imgView);

        imageCache.addGifUriToCache(filename, uri);


        MessageMedia newMessageMedia = new MessageMedia(uri, filename, "image/gif");
        messageMediaHashMap.put(filename, newMessageMedia);
        imgView.setTag(newMessageMedia);
        attachClickListener(imgView);
    }

    private void loadJPG(ImageView imgView, Uri uri, String filename)
    {
        SimpleTarget target = new SimpleTarget<Bitmap>() {

            @Override
            public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
//                Drawable dr = new BitmapDrawable(chat.getResources(), bitmap);
                imgView.setImageBitmap(bitmap);
                imageCache.addBitmapToMemoryCache(filename, bitmap);
            }
        };

        imageCache.addBitmapToUriCache(filename, uri);


        MessageMedia newMessageMedia = new MessageMedia(uri, filename, "image/jpeg");
        messageMediaHashMap.put(filename, newMessageMedia);
        imgView.setTag(newMessageMedia);
        attachClickListener(imgView);

        if(ImageCache.isValidContextForGlide(context)) {
            Glide.with(context).asBitmap().load(uri).placeholder(R.drawable.ic_photo_black_24dp)
                    .error(R.drawable.ic_photo_black_24dp)
                    .apply(requestOptions)
                    .into(target);
        }
    }



}
