package com.telbound.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.telbound.models.Announcement;
import com.telbound.models.NotifModel;
import com.telbound.orca.R;

import java.util.ArrayList;
import java.util.List;

public class NotifAdapter extends BaseAdapter {

    private List<NotifModel> notificationList;
    private LayoutInflater inflater;
    private Activity activity;

    public NotifAdapter(Activity activity, List<NotifModel> notifList) {
        this.activity = activity;
        this.notificationList = notifList;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            assert inflater != null;
            convertView = inflater.inflate(R.layout.fragment_announcement_row, null);
        }

        NotifModel notif = notificationList.get(position);

//        TextView msg = convertView.findViewById(R.id.msg_txtview);
//         Check if this is already read letter
//
//        msg.setText(notif.getMessage());

        return null;
    }

}

