package com.telbound.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.text.HtmlCompat;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.telbound.fragments.main.ReportsFragment;
import com.telbound.models.Message;
import com.telbound.models.Report;
import com.telbound.orca.MainActivity;
import com.telbound.orca.R;
import com.telbound.utils.PaddingBackgroundColorSpan;

import org.jsoup.Jsoup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ReportsAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater layoutInflater;
    private List<Report> reportList;
    private ReportsFragment reportsFragment;
    public HashMap<String, Message> lastMessages = new HashMap<>();

    public ReportsAdapter(Activity activity, List<Report> reportList, ReportsFragment reportsFragment) {
        this.activity = activity;
        this.reportList = reportList;
        this.reportsFragment = reportsFragment;
    }

    @Override
    public int getCount() {
        return reportList.size();
    }

    @Override
    public Object getItem(int position) {
        return reportList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
//            assert layoutInflater != null;
            convertView = layoutInflater.inflate(R.layout.fragment_reports_row, null);
        }

        TextView report_id = convertView.findViewById(R.id.reportId);
        TextView thread =  convertView.findViewById(R.id.thread);
//        TextView lastMessageDate = convertView.findViewById(R.id.lastMessageDate);
        TextView categories = convertView.findViewById(R.id.categories);
        TextView status_text = convertView.findViewById(R.id.status_text);
        TextView latest_message = convertView.findViewById(R.id.last_message_text);
        ImageView status_icon = convertView.findViewById(R.id.status_icon);
        TextView lastMessageDate = convertView.findViewById(R.id.lastMessageDate);


        Report report = reportList.get(position);

        convertView.setTag(report.getReportId());

        if(lastMessages.containsKey(report.getReportId())) {
            latest_message.setText(lastMessages.get(report.getReportId()).getMessage());
            lastMessageDate.setText(lastMessages.get(report.getReportId()).getDate().toString().split("GMT")[0]);
            latest_message.setVisibility(View.VISIBLE);
        }
        else
        {
            latest_message.setVisibility(View.GONE);
            lastMessageDate.setText("");
//            fetchLastMessage(report);
        }

        String statusText = reportsFragment.report_status_text[report.getStatus()];
        Spannable status_span = new SpannableString(statusText);
        status_span.setSpan(new ForegroundColorSpan(Color.parseColor(reportsFragment.report_status_colors[report.getStatus()])), 0, statusText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        status_text.setText(status_span);

        status_icon.setImageResource(reportsFragment.report_status_icons[report.getStatus()]);

        report_id.setText(report.getReportId());

        String textCategories = "";
        for (int i = 0; i < report.getCategories().size(); i++) {

            String category_name = report.getCategories().get(i).getName() + (i<report.getCategories().size()-1? ", " : "");

            textCategories += category_name;
        }

        categories.setText(textCategories);

        return convertView;
    }
}
