package com.telbound.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageReference;
import com.telbound.fragments.main.AnnouncementFragment;
import com.telbound.models.Announcement;
import com.telbound.orca.R;

import java.util.List;

public class AnnouncementAdapter extends BaseAdapter {

    private Activity activity;
    private List<Announcement> announcementList;
    private LayoutInflater inflater;


    private AnnouncementFragment myFragment;
    private FirebaseStorage storage;


    public AnnouncementAdapter(Activity activity, List<Announcement> announcementList, AnnouncementFragment myFragment) {
        this.activity = activity;
        this.announcementList = announcementList;
        this.myFragment = myFragment;
        storage = FirebaseStorage.getInstance();
    }

    @Override
    public int getCount() {
        return announcementList.size();
    }

    @Override
    public Object getItem(int location) {
        return announcementList.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            assert inflater != null;
            convertView = inflater.inflate(R.layout.fragment_announcement_row, null);
        }

        TextView title = convertView.findViewById(R.id.title);
//        TextView content =  convertView.findViewById(R.id.content);
        TextView start_date = convertView.findViewById(R.id.lastMessageDate);
        ImageView imageView = convertView.findViewById(R.id.header_image);

        Announcement announcement = announcementList.get(position);

        title.setText(announcement.getTitle());
        String[] splittedSDate = announcement.getStart_date().toString().split(" ");
        String[] splittedEDate = announcement.getEnd_date().toString().split(" ");
        start_date.setText(splittedSDate[0] + " " + splittedSDate[1] + " " + splittedSDate[2] + " - " + splittedEDate[0] + " " + splittedEDate[1] + " " + splittedEDate[2]);

        getSetHeaderImage(announcement.key, imageView);
        return convertView;
    }

    private void getSetHeaderImage(String key, ImageView imageView)
    {
        if(myFragment.headerImages.containsKey(key))
        {
            Glide.with(activity).load(myFragment.headerImages.get(key)).into(imageView);
        }
        else
        {
            try {

                StorageReference storageRef = storage.getReference();


                StorageReference listRef = storageRef.child("announcements/" + key);

                listRef.listAll()
                        .addOnSuccessListener(new OnSuccessListener<ListResult>() {
                            @Override
                            public void onSuccess(ListResult listResult) {

                                if(listResult.getItems().size() > 0)
                                {
                                    listResult.getItems().get(0).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            myFragment.headerImages.put(key, uri.toString());
                                            Glide.with(activity).load(myFragment.headerImages.get(key)).into(imageView);
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Glide.with(activity).load(R.drawable.ncst_logo_256px).into(imageView);
                                        }
                                    });
                                }
                                else
                                {
                                    Glide.with(activity).load(R.drawable.ncst_logo_256px).into(imageView);
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // Uh-oh, an error occurred!
                                Glide.with(activity).load(R.drawable.ncst_logo_256px).into(imageView);
                            }
                        });
            }
            catch (Exception e) {
                getSetHeaderImage(key, imageView);
            }
        }
    }
}
