package com.telbound.adapters;

import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.telbound.fragments.main.dummy.DummyContent.DummyItem;
import com.telbound.models.NotifModel;
import com.telbound.orca.MainActivity;
import com.telbound.orca.R;
import com.telbound.utils.GlideImageGetter;

import java.util.List;


public class MyNotifModelRecyclerViewAdapter extends RecyclerView.Adapter<MyNotifModelRecyclerViewAdapter.ViewHolder> {

    private final List<NotifModel> mValues;
    private View.OnClickListener mOnClickListener;
    private Activity activity;
    private Context context;

    public MyNotifModelRecyclerViewAdapter(List<NotifModel> items, Context context, Activity activity) {
        mValues = items;
        this.context = context;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.notif_entry_row, parent, false);

        mOnClickListener = new MyOnClickListener(layoutInflater, context, activity);
        view.setOnClickListener(mOnClickListener);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mDate.setText(mValues.get(position).getDateSent().toString().split("GMT")[0]);
        holder.mTitle.setText(mValues.get(position).getTitle());
        holder.mView.setTag(holder.mItem);

        if(!MainActivity.instance.viewedNotifications.contains(holder.mItem.key))
            holder.mNewIndicator.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mDate;
        public final TextView mTitle;
        public final ImageView mNewIndicator;
        public NotifModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mDate = (TextView) view.findViewById(R.id.notif_date);
            mTitle = (TextView) view.findViewById(R.id.notif_title);
            mNewIndicator = view.findViewById(R.id.img_new_notif);

//            mView.setOnClickListener();
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + "'";
        }
    }

    public class MyOnClickListener implements View.OnClickListener
    {
        private LayoutInflater layoutInflater;
        private Context context;
        private Activity activity;

        public MyOnClickListener(LayoutInflater layoutInflater, Context context, Activity activity) {
            this.layoutInflater = layoutInflater;
            this.context = context;
            this.activity = activity;
        }

        @Override
        public void onClick(View view) {
            NotifModel notifModel = (NotifModel) view.getTag();

            if(notifModel != null)
            {
                View dialoglayout = layoutInflater.inflate(R.layout.notif_view_layout, null);
                TextView title = dialoglayout.findViewById(R.id.title);
                title.setText(notifModel.getTitle());

                TextView content = dialoglayout.findViewById(R.id.content);
                content.setText(Html.fromHtml(notifModel.getMessage(), new GlideImageGetter(content, false, false, null), null));

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setView(dialoglayout);

                builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });


                builder.show();

                if(!MainActivity.instance.viewedNotifications.contains(notifModel.key)) {
                    MainActivity.instance.viewedNotifications.add(notifModel.key);

                    MainActivity.instance.totalNotifs = Math.max(0, MainActivity.instance.totalNotifs-1);
                    MainActivity.instance.notifCounter.setText(String.valueOf(MainActivity.instance.totalNotifs));

                    MainActivity.instance.savePreference();
                }

                view.findViewById(R.id.img_new_notif).setVisibility(View.GONE);
            }
        }
    }
}