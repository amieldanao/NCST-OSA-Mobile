package com.telbound.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.telbound.models.HelpModel;
import com.telbound.orca.MainActivity;
import com.telbound.orca.R;

import java.util.List;

public class HelpAdapter extends BaseAdapter {
    private static final String TAG = "myLogTag";

    private Activity activity;
    private List<HelpModel> helpModelList;
    private LayoutInflater inflater;

    public HelpAdapter(Activity activity, List<HelpModel> helpModelList) {
        this.activity = activity;
        this.helpModelList = helpModelList;
    }

    @Override
    public int getCount() {
        return helpModelList.size();
    }

    @Override
    public Object getItem(int location) {
        return helpModelList.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            assert inflater != null;
            convertView = inflater.inflate(R.layout.fragment_help_row, null);
        }

        TextView section = convertView.findViewById(R.id.text_section);

        HelpModel thisHelp = helpModelList.get(position);

        section.setText(thisHelp.getSection());

        Log.d(TAG, section.getText().toString());

//        View view = super.getView(position, convertView, parent);

//        TextView textView = (TextView) view.findViewById(R.id.text_section);
//        textView.setHeight(30);
//        textView.setMinimumHeight(30);

        /*YOUR CHOICE OF COLOR*/
//        textView.setTextColor(Color.BLACK);

        return convertView;
    }
}
