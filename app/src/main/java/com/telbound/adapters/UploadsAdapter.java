package com.telbound.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.telbound.models.Upload;
import com.telbound.orca.Chat;
import com.telbound.orca.R;

import java.util.List;

public class UploadsAdapter extends ArrayAdapter<Upload> {

    private static final String TAG = "myLogTag";

    private List<Upload> uploadList;
    private Context context;

    private Chat chat;

    public UploadsAdapter(Context context, List<Upload> uploadList, Chat c) {
        super(context, 0, uploadList);
        this.context = context;
        this.uploadList=uploadList;
        this.chat = c;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View convertView = inflater.inflate(R.layout.upload_progress_entry, parent, false);

        TextView txt_filename = convertView.findViewById(R.id.txt_upload_filename);
        TextView txt_filesize =  convertView.findViewById(R.id.txt_upload_filesize);
        ProgressBar progress = convertView.findViewById(R.id.upload_progressbar);
        ImageView cancelUpload = convertView.findViewById(R.id.img_upload_cancel);

        Upload thisUpload = uploadList.get(position);
        txt_filename.setText(thisUpload.getOrigFilenameOnly());

        cancelUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chat.cancelUpload(thisUpload.getFilename(), thisUpload.getOrigFilenameOnly());
            }
        });


//        if(chat.allUploadViews.containsKey(thisUpload.getFilename()))
//            chat.allUploadViews.remove(thisUpload.getFilename());
//        chat.allUploadViews.put(thisUpload.getFilename(), convertView);

        return convertView;
    }


}
