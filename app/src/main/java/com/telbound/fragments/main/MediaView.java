package com.telbound.fragments.main;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.telbound.models.MessageMedia;
import com.telbound.orca.Chat;
import com.telbound.orca.R;

import java.net.URI;



public class MediaView extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String TAG = "myLogTag";

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private Uri uri;
    private String filename;
    private String mimeType;

    private ImageView theViewer;
    private VideoView videoView;
    private TextView txt_url;

    private MediaController ctlr;

    private OnFragmentInteractionListener mListener;

    public MediaView() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param messageMedia Parameter 1.
//     * @param param2 Parameter 2.
     * @return A new instance of fragment MediaView.
     */
    // TODO: Rename and change types and number of parameters
    public static MediaView newInstance(MessageMedia messageMedia) {
        MediaView fragment = new MediaView();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, messageMedia.getUri().toString());
        args.putString(ARG_PARAM2, messageMedia.getFilename());
        args.putString(ARG_PARAM3, messageMedia.getMimetype());

        fragment.setArguments(args);


        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            uri = Uri.parse((getArguments().getString(ARG_PARAM1)));
            filename = getArguments().getString(ARG_PARAM2);
            mimeType = getArguments().getString(ARG_PARAM3);
        }

        hideKeyboard(Chat.instance);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View parent = inflater.inflate(R.layout.fragment_media_view, container, false);


        parent.setClickable(true);
        parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        theViewer = parent.findViewById(R.id.theViewer);
        videoView = parent.findViewById(R.id.videoView);

        if(mimeType.contains("image"))
        {
            if(mimeType.contains("gif"))
                loadGIF();
            else
                loadJPG();
        }
        else if(mimeType.contains("video"))
        {
            loadVideo();
        }
        return parent;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void loadVideo()
    {
        videoView.setVisibility(View.VISIBLE);
        videoView.setVideoURI(uri);
        videoView.start();
        ctlr = new MediaController(Chat.context);
        ctlr.setMediaPlayer(videoView);
        videoView.setMediaController(ctlr);
        videoView.requestFocus();
    }

    private void loadGIF()
    {
        theViewer.setVisibility(View.VISIBLE);
        Glide
                .with(Chat.context)
                .asGif()
                .load(uri)
                .error(R.drawable.ic_photo_black_24dp)
                .into(theViewer);
    }

    private void loadJPG()
    {
        theViewer.setVisibility(View.VISIBLE);
        Bitmap tryBitmap = Chat.messageAdapater.imageCache.getBitmapFromMemCache(filename);

        if(tryBitmap != null)
        {
            theViewer.setImageBitmap(tryBitmap);
        }
        else
        {
            Glide.with(Chat.context).asBitmap().load(uri).placeholder(R.drawable.ic_photo_black_24dp)
                    .error(R.drawable.ic_photo_black_24dp)
                    .into(theViewer);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
