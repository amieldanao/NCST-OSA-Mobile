package com.telbound.fragments.main;


import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.telbound.adapters.AnnouncementAdapter;
import com.telbound.models.Announcement;
import com.telbound.orca.MainActivity;
import com.telbound.orca.R;
import com.telbound.utils.GlideImageGetter;
import com.telbound.utils.UtilSession;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnnouncementFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "myLogTag";
    private View view;

    private AnnouncementAdapter announcementAdapter;
    private List<Announcement> announcementList = new ArrayList<>();

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private FirebaseFirestore db;
    private ListenerRegistration announcementListener;

//    private AlertDialog newsPreview;

    public Map<String, String> headerImages = new HashMap<String, String>();


    public ListView listView;

    public AnnouncementFragment() {
        // Required empty public constructor
        db = FirebaseFirestore.getInstance();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_announcements, container, false);

        MainActivity.instance.findViewById(R.id.fab_options).setVisibility(View.VISIBLE);

        MainActivity.instance.initToolbar(view);
        SpannableString s = new SpannableString(getString(R.string.nav_home));
        s.setSpan(new TypefaceSpan("open_sans_bold.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        MainActivity.instance.myToolbar.setTitle(s);

        initViews();
        return view;
    }

    private void initViews() {


        listView = view.findViewById(R.id.announcement_list);
        announcementAdapter = new AnnouncementAdapter(getActivity(), announcementList, this);
        listView.setAdapter(announcementAdapter);

        listView.setOnItemClickListener((parent, lvlview, position, id) -> {
            Announcement a = (Announcement) announcementAdapter.getItem(position);

            LayoutInflater inflater = getLayoutInflater();
            View dialoglayout = inflater.inflate(R.layout.news_layout, null);
            TextView title = dialoglayout.findViewById(R.id.title);
            title.setText(a.getTitle());

            if(headerImages.containsKey(a.key)) {
                ImageView headerImage = dialoglayout.findViewById(R.id.headerImage);
                Glide.with(getActivity()).load(headerImages.get(a.key)).into(headerImage);
            }


            TextView content = dialoglayout.findViewById(R.id.content);
            content.setText(Html.fromHtml(a.getContent(), new GlideImageGetter(content, false, true, null), null));

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setView(dialoglayout);

            builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });


            builder.show();

        });

        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.post(() -> {
        });

        fetchAnnouncements();
    }

    private void showToast(String text)
    {
        Toast.makeText(MainActivity.myContext, text, Toast.LENGTH_LONG).show();
    }

    private boolean hasAnnouncementAlready(Announcement announcement)
    {
        for (Announcement a : announcementList) {
            if(a.getTitle().equals(announcement.getTitle()))
                return true;
        };

        return false;
    }

    private int getAnnouncementIndex(Announcement announcement)
    {
        for (int i = 0; i < announcementList.size(); i++) {
            if(announcement.key.equals(announcementList.get(i).key))
                return i;
        }

        return -1;
    }

    private void fetchAnnouncements()
    {
        announcementList.clear();
        progressBar(true);

        Query query = db.collection("announcements")
                .whereEqualTo("active", true);
//                .orderBy("start_date", Query.Direction.DESCENDING);
//                .whereGreaterThanOrEqualTo("start_date", new Date())
//                .whereLessThanOrEqualTo("end_date", new Date());

        if (announcementListener != null)
            announcementListener.remove();

        announcementListener = query.addSnapshotListener(
        (snapshots, e) -> {
            if (e != null) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {

//                            Log.w(TAG, "listen:error", e);
//                            showToast(e.getMessage());
                    progressBar(false);
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                return;
            }

            if(snapshots.getDocuments().size() == 0)
            {
//                        showToast("No Announcements.");
                progressBar(false);
                announcementList.clear();
                announcementAdapter.notifyDataSetChanged();
                mSwipeRefreshLayout.setRefreshing(false);
                return;
            }

            for (DocumentChange dc : snapshots.getDocumentChanges()) {

                Date thisStartDate = dc.getDocument().getDate("start_date");
                Date thisEndDate = dc.getDocument().getDate("end_date");
                Date today = new Date();

//                Log.d(TAG, thisStartDate.toString());
//                Log.d(TAG, today.toString());
//                Log.d(TAG, thisEndDate.toString());

                boolean isWithingDateRange = today.after(thisStartDate) && today.before(thisEndDate);

//                Log.d(TAG, dc.getDocument().getString("title") + " - isWithingDateRange = " +isWithingDateRange );
                // Lets only add or update announcements that is within date range
                // after
                if(isWithingDateRange) {

                    switch (dc.getType()) {


                        case ADDED:

                            Log.d(TAG, "New report: " + dc.getDocument().getData());
                            Announcement thisAnnouncement = dc.getDocument().toObject(Announcement.class);
                            thisAnnouncement.key = dc.getDocument().getId();

                            if (!hasAnnouncementAlready(thisAnnouncement))
                                announcementList.add(thisAnnouncement);


                            break;
                        case MODIFIED:

                            Log.d(TAG, "Modified report: " + dc.getDocument().getData());
                            Announcement thisAnnouncementModified = dc.getDocument().toObject(Announcement.class);
                            thisAnnouncementModified.key = dc.getDocument().getId();

                            int index = getAnnouncementIndex(thisAnnouncementModified);

                            if (index > -1)
                                announcementList.set(index, thisAnnouncementModified);
                            else
                                announcementList.add(thisAnnouncementModified);


                            break;

                    }

                }

                if (dc.getType() == DocumentChange.Type.REMOVED) {

                    Log.d(TAG, "Removed report: " + dc.getDocument().getData());

                    Announcement thisAnnouncementRemoved = dc.getDocument().toObject(Announcement.class);
                    thisAnnouncementRemoved.key = dc.getDocument().getId();

                    int indexOfRemoved = getAnnouncementIndex(thisAnnouncementRemoved);

                    if (indexOfRemoved > -1)
                        announcementList.remove(indexOfRemoved);
                    if (headerImages.containsKey(thisAnnouncementRemoved.key))
                        headerImages.remove(thisAnnouncementRemoved.key);
                }

            }



            Log.d(TAG, "done loading my announcements");
            progressBar(false);
            mSwipeRefreshLayout.setRefreshing(false);
            announcementAdapter.notifyDataSetChanged();


        });
    }

    private void progressBar(boolean show) {
        View support_layout = view.findViewById(R.id.support_layout);

        if (show == false) {
            support_layout.setVisibility(View.GONE);
        } else {
            support_layout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        fetchAnnouncements();
    }
}
