package com.telbound.fragments.main;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.telbound.orca.MainActivity;
import com.telbound.orca.R;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class AlertsFragment extends Fragment {
    private static final String TAG = AlertsFragment.class.getSimpleName();
    private View view;


    public AlertsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_alerts, container, false);

        initViews();

        return view;
    }

    private void initViews() {
        ((MainActivity) Objects.requireNonNull(getActivity())).setTitle(getString(R.string.nav_notification));
    }

}
