package com.telbound.fragments.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.telbound.adapters.MyNotifModelRecyclerViewAdapter;
import com.telbound.models.Announcement;
import com.telbound.orca.MainActivity;
import com.telbound.orca.R;
import com.telbound.utils.GlideImageGetter;

/**
 * A fragment representing a list of Items.
 */
public class NotifFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private MyNotifModelRecyclerViewAdapter myAdapter;


    public NotifFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static NotifFragment newInstance(int columnCount) {
        NotifFragment fragment = new NotifFragment();
        Bundle args = new Bundle();
//        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (getArguments() != null) {
//            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);

        // Set the adapter
//        if (view instanceof RecyclerView) {
//            Context context = view.getContext();
//
//        }

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.notif_list_adapter);
        myAdapter = new MyNotifModelRecyclerViewAdapter(MainActivity.instance.notifList, view.getContext(), getActivity());
        MainActivity.instance.myAdapter = myAdapter;
        recyclerView.setAdapter(myAdapter);


        MainActivity.instance.myToolbar = view.findViewById(R.id.main_toolbar);

        MainActivity.instance.mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        MainActivity.instance.mSwipeRefreshLayout.setOnRefreshListener(this);
        MainActivity.instance.mSwipeRefreshLayout.post(() -> {
        });

        SpannableString s = new SpannableString("Notifications");
        s.setSpan(new TypefaceSpan("open_sans_bold.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        MainActivity.instance.setTitle(s);
        MainActivity.instance.setSupportActionBar(MainActivity.instance.myToolbar);

//        Button backButton = view.findViewById(R.id.btn_back_button);
//        backButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                getActivity().onBackPressed();
//                getActivity().getFragmentManager().popBackStack();
//            }
//        });

//        MainActivity.instance.attachNotificationCallback();

        return view;
    }

    @Override
    public void onRefresh() {
//        MainActivity.instance.attachNotificationCallback();
        MainActivity.instance.mSwipeRefreshLayout.setRefreshing(false);
    }
}