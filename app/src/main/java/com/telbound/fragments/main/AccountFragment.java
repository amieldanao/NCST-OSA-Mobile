package com.telbound.fragments.main;


import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEventSource;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.fxn.pix.Options;
import com.fxn.utility.PermUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.telbound.orca.AppController;
import com.telbound.orca.AuthActivity;
import com.telbound.orca.Chat;
import com.telbound.orca.ImagePickerActivity;
import com.telbound.orca.MainActivity;
import com.telbound.orca.R;
import com.telbound.utils.MyUtilMedia;
import com.telbound.utils.PixCustom;
import com.telbound.utils.UtilConstants;
import com.telbound.utils.UtilSession;
import com.telbound.utils.UtilVolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.telbound.utils.UtilsData.toTitleCase;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {
    private static final String TAG = "myLogTag";
    private View view;

//    private CircleImageView profile;

    private CircleImageView profile;

    private UtilSession utilSession;

    private static final int REQUEST_IMAGE = 100;
    private Button changePassBtn;
    private View support_layout;
    private final int pixImageRequest = 666;


    public AccountFragment() {
        // Required empty public constructor
    }

    private void initGetProfilePic()
    {
        Bitmap tryPic = MainActivity.instance.getBitmapFromMemCache(AuthActivity.instance.loggedStudent.getID());
//        profile.setDrawingCacheEnabled(true);

        if(tryPic == null) {

            Log.d(TAG, "begin loading profile pic");

            StorageReference reference = FirebaseStorage.getInstance().getReference()
                    .child("profileImages")
                    .child(AuthActivity.instance.loggedStudent.getID() + ".jpeg");

            reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    if(isAdded())
                        loadProfile(uri);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
//                Log.d(TAG, "Profile pic not found!");
//                    AuthActivity.instance.profilePic = null;
//                    showToast("Profile pic not found!");
                }
            });
        }
        else
        {
            if(profile != null) {
                profile.setImageBitmap(tryPic);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account, container, false);

        Log.d(TAG, "OnCreateView");
        initViews();
        initActions();

        initGetProfilePic();

        return view;
    }

    private void progressBar(boolean show) {
        View support_layout = view.findViewById(R.id.support_layout);

        if (show == false) {
            support_layout.setVisibility(View.GONE);
        } else {
            support_layout.setVisibility(View.VISIBLE);
        }
    }

    private void initActions() {
        MainActivity.instance.findViewById(R.id.fab_options).setVisibility(View.GONE);

        Button logout = view.findViewById(R.id.btn_logout);
        logout.setOnClickListener(v -> {

            AuthActivity.instance.loggedStudent = null;
            MainActivity.instance.clearBitmaps();
            MainActivity.instance.clearNotifications();
            FirebaseAuth.getInstance().signOut();
        });


        support_layout = view.findViewById(R.id.support_layout);

        changePassBtn = view.findViewById(R.id.account_password);
        changePassBtn.setOnClickListener(v -> {
//            changePassBtn.setEnabled(false);

            // confirm change password

            try {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                // Get the layout inflater
                LayoutInflater inflater = requireActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                View parent = inflater.inflate(R.layout.activity_password, null);
                builder.setView(parent)
                .setTitle("Enter your new password")
                // Add action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        progressBar(true);

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        EditText editText = parent.findViewById(R.id.passwordEdit);
                        EditText passwordConfirm = parent.findViewById(R.id.passwordConfirm);
                        String newPassword = editText.getText().toString();
                        String confirmPassword = passwordConfirm.getText().toString();

                        if(!confirmPassword.equals(newPassword))
                        {
                            showToast("Password doesn't match!");
                            progressBar(false);
                            return;
                        }

                        user.updatePassword(newPassword)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                showToast("Password changed successfully");

                                FirebaseAuth.getInstance().signOut();
                                progressBar(false);
                                dialog.dismiss();
                            }
                        })
                        .addOnFailureListener(e -> {
                            showToast(e.getMessage());
                            progressBar(false);
                            dialog.dismiss();
                        });
                    }
                })
                .setNegativeButton("Cancel", (dialog, id) ->{
                        dialog.cancel();
                        progressBar(false);
                    }
                );

                AlertDialog alert = builder.create();
                alert.show();

            }
            catch (Exception e)
            {
                showToast(e.getMessage());
                progressBar(false);
            }
        });

        profile.setOnClickListener(v -> onProfileImageClick());

    }

    private void showToast(String message){
        Toast.makeText(MainActivity.myContext, message, Toast.LENGTH_LONG).show();
    }

    private void loadProfile(Uri url) {
        Log.d(TAG, "Image cache path: " + url);



        SimpleTarget target = new SimpleTarget<Bitmap>() {

            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                profile.setImageBitmap(resource);
                if(AuthActivity.instance.loggedStudent != null)
                    MainActivity.instance.addBitmapToMemoryCache(AuthActivity.instance.loggedStudent.getID(), getBitmapFromView(profile));
                Log.d(TAG, "done placing glide");
            }
        };

        Glide.with(this).asBitmap().load(url).placeholder(R.drawable.noimage)
                .error(R.drawable.noimage)
                /*.listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        Log.d(TAG, "failed placing glide!");
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        MainActivity.instance.addBitmapToMemoryCache(AuthActivity.instance.loggedStudent.getStudent_id(), getBitmapFromView(profile));
                        Log.d(TAG, "done placing glide");
                        return false;
                    }
                })*/
                .into(target);

//        AuthActivity.instance.profilePic = getBitmapFromView(profile);


    }

    public Bitmap getBitmapFromView(View view)
    {
        Log.d(TAG, "imgWidth = " + view.getWidth());
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private void uploadNewProfile(Bitmap bitmap)
    {
        profile.setImageBitmap(bitmap);

        profile.invalidate();
//        profile.setDrawingCacheEnabled(true);
//        BitmapDrawable drawable = (BitmapDrawable) profile.getDrawable();
        Bitmap theBitmap = getBitmapFromView(profile);
//        AuthActivity.instance.profilePic = theBitmap;
        MainActivity.instance.addBitmapToMemoryCache(AuthActivity.instance.loggedStudent.getID(), theBitmap);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        theBitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);

        StorageReference reference = FirebaseStorage.getInstance().getReference()
                .child("profileImages")
                .child(AuthActivity.instance.loggedStudent.getID() + ".jpeg");

        reference.putBytes(baos.toByteArray()).addOnSuccessListener(taskSnapshot -> Log.d(TAG, "Done upload profile pic")).addOnFailureListener(e -> Log.e(TAG, "Profile pic fail upload : ", e.getCause()));
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", TAG, null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void onProfileImageClick() {
        Dexter.withActivity(getActivity())
            .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport report) {
                    if (report.areAllPermissionsGranted()) {
                        showImagePickerOptions();
//                        startPixImagePicker();

                    }

                    if (report.isAnyPermissionPermanentlyDenied()) {
                        showSettingsDialog();
                    }
                }

                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                    token.continuePermissionRequest();
                }
            }).check();
    }

//    private void startPixImagePicker()
//    {
//        Options options = Options.init()
//                .setRequestCode(pixImageRequest)                                           //Request code for activity results
//                .setCount(1)                                                   //Number of images to restict selection count
//                .setFrontfacing(false)                                          //Pre selected Image Urls
//                .setExcludeVideos(true)                                       //Option to exclude videos
//                .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)     //Orientaion
//                .setPath("/storage/emulated/0/DCIM/");                                       //Custom Path For media Storage
//
//        PixCustom.start(this, options);
//    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    startPixImagePicker();
//
//                } else {
//                    Toast.makeText(getContext(), "Approve permissions to open Pix ImagePicker", Toast.LENGTH_LONG).show();
//                }
//                return;
//            }
//        }
//    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(getContext(), new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
            Intent intent = new Intent(getContext(), ImagePickerActivity.class);
            intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

            // setting aspect ratio
            intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
            intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
            intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

            // setting maximum bitmap width and height
            intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
            intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
            intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

            startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {

        Intent intent = new Intent(getContext(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == Activity.RESULT_OK && requestCode == pixImageRequest) {
//            ArrayList<String> selectedMedias = data.getStringArrayListExtra(PixCustom.IMAGE_RESULTS);
//
//            Uri uri = Uri.parse(selectedMedias.get(0));
//            try {
//                // You can update this bitmap to your server
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(
//                        Objects.requireNonNull(getContext()).getContentResolver(), uri);
//
////                    assert uri != null;
////                    HashMap<String, String> user = utilSession.getDetails();
//                uploadNewProfile(bitmap);
//                //loadProfile(uri.toString());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Uri uri = data.getParcelableExtra("path");
                try {
                    // You can update this bitmap to your server
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(
                            Objects.requireNonNull(getContext()).getContentResolver(), uri);

//                    assert uri != null;
//                    HashMap<String, String> user = utilSession.getDetails();
                    uploadNewProfile(bitmap);
                    //loadProfile(uri.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void initViews() {
//        progressBar();
//        ((MainActivity) Objects.requireNonNull(getActivity())).setTitle(getString(R.string.nav_account));

        SpannableString s = new SpannableString(getString(R.string.nav_account));
        s.setSpan(new TypefaceSpan("open_sans_bold.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        ((MainActivity) Objects.requireNonNull(getActivity())).setTitle(s);

        profile = view.findViewById(R.id.profile_image);
        TextView name = view.findViewById(R.id.account_name);
        TextView email = view.findViewById(R.id.account_email);
        TextView id = view.findViewById(R.id.account_id);
        TextView phone = view.findViewById(R.id.account_phone);
        TextView address = view.findViewById(R.id.account_address);

        if (AuthActivity.instance != null && AuthActivity.instance.loggedStudent != null) {
            String UserName = toTitleCase(AuthActivity.instance.loggedStudent.getFirst_name() + ' ' + AuthActivity.instance.loggedStudent.getLast_name());
            String emailString = AuthActivity.instance.loggedStudent.getEmail();
            name.setText(UserName);
            email.setText(emailString);
            id.setText(AuthActivity.instance.loggedStudent.getID());
            phone.setText(AuthActivity.instance.loggedStudent.getContact_number());
            address.setText(AuthActivity.instance.loggedStudent.getAddress());
        }

    }

    private void progressBar() {
        View support_layout = view.findViewById(R.id.support_layout);

        if (support_layout.isShown()) {
            support_layout.setVisibility(View.GONE);
        } else {
            support_layout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        ImagePickerActivity.hideImagePickOptionDialog();
    }
}
