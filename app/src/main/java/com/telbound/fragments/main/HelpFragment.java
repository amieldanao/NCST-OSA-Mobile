package com.telbound.fragments.main;


import android.app.ActionBar;
import android.app.AlertDialog;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.telbound.adapters.HelpAdapter;
import com.telbound.models.HelpModel;
import com.telbound.orca.MainActivity;
import com.telbound.orca.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class HelpFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "myLogTag";
    private View view;

    private HelpAdapter helpAdapter;
    private List<HelpModel> guidelineList = new ArrayList<>();

    private SwipeRefreshLayout mSwipeRefreshLayout;
    public ListView listView;

    private FirebaseFirestore db;
    private ListenerRegistration guidelineListener;

    public HelpFragment() {
        // Required empty public constructor
        db = FirebaseFirestore.getInstance();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_help, container, false);
        MainActivity.instance.findViewById(R.id.fab_options).setVisibility(View.VISIBLE);

        MainActivity.instance.initToolbar(view);

        SpannableString s = new SpannableString("Handbook");
        s.setSpan(new TypefaceSpan("open_sans_bold.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        MainActivity.instance.myToolbar.setTitle(s);

        initViews();

        return view;
    }


    private void initViews() {




//        ((MainActivity) Objects.requireNonNull(getActivity())).setTitle(s);

        listView = view.findViewById(R.id.guideline_list);
        helpAdapter = new HelpAdapter(getActivity(), guidelineList);
        listView.setAdapter(helpAdapter);

        listView.setOnItemClickListener((parent, lvlview, position, id) -> {
            HelpModel helpModel = (HelpModel) helpAdapter.getItem(position);

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//            builder.setView(dialoglayout);
            builder.setTitle(helpModel.getSection());
            builder.setMessage(Html.fromHtml(helpModel.getContent()));

            builder.setNegativeButton("Close", (dialogInterface, i) -> dialogInterface.dismiss());

            builder.show();

        });

        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.post(() -> {
        });

        fetchGuidelines();
    }



    private void fetchGuidelines()
    {
        guidelineList.clear();
        progressBar(true);

        Query query = db.collection("guidelines").orderBy("section");
//                .whereGreaterThanOrEqualTo("start_date", new Date())
//                .whereLessThanOrEqualTo("end_date", new Date());

        if (guidelineListener != null)
            guidelineListener.remove();

        guidelineListener = query.addSnapshotListener(
                (snapshots, e) -> {
                    if (e != null) {
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        if (user != null) {

                            Log.w(TAG, "listen:error", e);
                            showToast(e.getMessage());
                            progressBar(false);
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        return;
                    }

                    if(snapshots.getDocuments().size() == 0)
                    {
                        showToast("No Guidelines.");
                        progressBar(false);
                        guidelineList.clear();
                        helpAdapter.notifyDataSetChanged();
                        mSwipeRefreshLayout.setRefreshing(false);
                        return;
                    }

                    for (DocumentChange dc : snapshots.getDocumentChanges()) {

                        switch (dc.getType()) {
                            case ADDED:

//                                Log.d(TAG, "New report: " + dc.getDocument().getData());
                                HelpModel thisHelp = dc.getDocument().toObject(HelpModel.class);
                                thisHelp.key = dc.getDocument().getId();

                                if (!hasHelpAlready(thisHelp))
                                    guidelineList.add(thisHelp);


                                break;
                            case MODIFIED:

//                                Log.d(TAG, "Modified report: " + dc.getDocument().getData());
                                HelpModel thisHelpModified = dc.getDocument().toObject(HelpModel.class);
                                thisHelpModified.key = dc.getDocument().getId();

                                int index = getHelpIndex(thisHelpModified);

                                if (index > -1)
                                    guidelineList.set(index, thisHelpModified);
                                else
                                    guidelineList.add(thisHelpModified);


                                break;
                            case REMOVED:

//                                Log.d(TAG, "Removed report: " + dc.getDocument().getData());

                                HelpModel thisHelpRemoved = dc.getDocument().toObject(HelpModel.class);
                                thisHelpRemoved.key = dc.getDocument().getId();

                                int indexOfRemoved = getHelpIndex(thisHelpRemoved);

                                if (indexOfRemoved > -1)
                                    guidelineList.remove(indexOfRemoved);

                                break;
                        }

                    }




                    Log.d(TAG, "done loading my guidelines");
                    progressBar(false);
                    mSwipeRefreshLayout.setRefreshing(false);
                    helpAdapter.notifyDataSetChanged();


                });
    }

    private boolean hasHelpAlready(HelpModel helpModel)
    {
        for (HelpModel g : guidelineList) {
            if(g.key.equals(helpModel.key))
                return true;
        };

        return false;
    }

    private int getHelpIndex(HelpModel helpModel)
    {
        for (int i = 0; i < guidelineList.size(); i++) {
            if(helpModel.key.equals(guidelineList.get(i).key))
                return i;
        }

        return -1;
    }

    private void showToast(String text)
    {
        Toast.makeText(MainActivity.myContext, text, Toast.LENGTH_LONG).show();
    }

    private void progressBar(boolean show) {
        View support_layout = view.findViewById(R.id.support_layout);

        if (show == false) {
            support_layout.setVisibility(View.GONE);
        } else {
            support_layout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        fetchGuidelines();
    }
}
