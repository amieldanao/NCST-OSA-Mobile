package com.telbound.fragments.main;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.telbound.adapters.ReportsAdapter;
import com.telbound.models.Message;
import com.telbound.models.Report;
import com.telbound.models.Student;
import com.telbound.orca.AppController;
import com.telbound.orca.AuthActivity;
import com.telbound.orca.Chat;
import com.telbound.orca.MainActivity;
import com.telbound.orca.R;
import com.telbound.utils.UtilConstants;
import com.telbound.utils.UtilSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "myLogTag";

    private View view;

    private ReportsAdapter reportsAdapter;
    private List<Report> reportList = new ArrayList<>();

    private Handler handler;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private final FirebaseFirestore db;
    private ListenerRegistration reportListener;
    private HashMap<String, ListenerRegistration> ReportMessagesListeners = new HashMap<String, ListenerRegistration>();

    public ReportsFragment() {
        db = FirebaseFirestore.getInstance();
    }

    public int[] report_status_icons = new int[4];
    public String[] report_status_colors = new String[4];
    public String[] report_status_text = new String[4];

    private MainActivity mainActivity;

    @Override
    public void onResume() {
        super.onResume();
        fetchMyReports();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_reports, container, false);

        MainActivity.instance.findViewById(R.id.fab_options).setVisibility(View.VISIBLE);
        MainActivity.instance.initToolbar(view);
        SpannableString s = new SpannableString(getString(R.string.nav_message));
        s.setSpan(new TypefaceSpan("open_sans_bold.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        MainActivity.instance.myToolbar.setTitle(s);

        report_status_icons[3] = R.drawable.ic_pending;
        report_status_icons[2] = R.drawable.ic_reported;
        report_status_icons[1] = R.drawable.ic_investigating;
        report_status_icons[0] = R.drawable.ic_case_closed;

        report_status_colors[3] = "#FFA4D4D1";
        report_status_colors[2] = "#00A4EA";
        report_status_colors[1] = "#ffff8800";
        report_status_colors[0] = "#FF00EA05";

        report_status_text[3] = "Pending...";
        report_status_text[2] = "Reported...";
        report_status_text[1] = "Investigating...";
        report_status_text[0] = "Case closed.";

        initViews();

        return view;
    }

    private void initViews() {

        ListView listView = view.findViewById(R.id.report_list);
        reportsAdapter = new ReportsAdapter(getActivity(), reportList, this);
        listView.setAdapter(reportsAdapter);

        listView.setOnItemClickListener((parent, lvlview, position, id) -> {
            Report r = (Report) reportsAdapter.getItem(position);

//            Log.d(TAG, (String) parent.getTag());
            if(r.getStatus() != 3) {
                Log.d(TAG, (String) lvlview.getTag());

                Intent i = new Intent(getContext(), Chat.class);
                i.putExtra("EXTRA_REPORT_ID", r.getReportId());
                i.putExtra("EXTRA_REPORT_KEY", r.key);
                startActivity(i);

                Log.d(TAG, "my id is " + id);
            }
            else
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("OSA is still verifying your report...")
                        .setTitle("Report Pending");
                AlertDialog dialog = builder.create();
                dialog.show();
            }

            //finish();
            //handler.removeCallbacksAndMessages(null);
        });

        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {
            }
        });

        fetchMyReports();
    }

    private static ArrayList<View> getViewsByTag(ViewGroup root, String tag){
        ArrayList<View> views = new ArrayList<View>();
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = root.getChildAt(i);
            if (child instanceof ViewGroup) {
                views.addAll(getViewsByTag((ViewGroup) child, tag));
            }

            final Object tagObj = child.getTag();
            if (tagObj != null && tagObj.equals(tag)) {
                views.add(child);
            }

        }
        return views;
    }

    private void showToast(String text)
    {
        Toast.makeText(MainActivity.myContext, text, Toast.LENGTH_LONG).show();
    }

    private void fetchMyReports()
    {
        reportList.clear();
        reportsAdapter.lastMessages.clear();
        progressBar(true);

        Query query = db.collection("reports")
//                .whereArrayContains("senders", AuthActivity.instance.loggedStudent.getEmail())
                .whereGreaterThan("status", 0)
                .orderBy("status")
                .orderBy("reportId", Query.Direction.DESCENDING);

        if (reportListener != null)
            reportListener.remove();

        reportListener = query.addSnapshotListener(
                (snapshots, e) -> {
                    if (e != null) {
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        if (user != null) {

                            Log.w(TAG, "listen:error", e);
//                            showToast(e.getMessage());
                            progressBar(false);
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                        return;
                    }

                    if(snapshots.getDocuments().size() == 0)
                    {
//                        showToast("No reports.");
                        progressBar(false);
                        reportList.clear();
                        reportsAdapter.notifyDataSetChanged();
                        mSwipeRefreshLayout.setRefreshing(false);
                        return;
                    }

                    for (DocumentChange dc : snapshots.getDocumentChanges()) {

                        Report thisReport = dc.getDocument().toObject(Report.class);
                        thisReport.key = dc.getDocument().getId();
                        int index = getReportIndex(thisReport);
                        switch (dc.getType()) {

                            case ADDED:

                                if(thisReport.getSenders().contains(AuthActivity.instance.loggedStudent.getEmail())) {


//                                    if (!hasReportAlready(thisReport))
//                                        reportList.add(thisReport);

                                    if (index > -1) {
                                        reportList.set(index, thisReport);
                                        Log.d(TAG, thisReport.key + " modified : status = " + thisReport.getStatus());
                                    }
                                    else
                                        reportList.add(thisReport);

//                                    if(mainActivity.viewedNotifications.contains(thisReport.key))
//                                    {
//                                        mainActivity.viewedNotifications.remove(thisReport.key);
//                                        Log.d(TAG, "Removed notification " + thisReport.reportId);
//                                        mainActivity.savePreference();
//                                    }

                                    if (ReportMessagesListeners.containsKey(thisReport.getReportId())) {
                                        ReportMessagesListeners.get(thisReport.getReportId()).remove();
                                        ReportMessagesListeners.remove(thisReport.getReportId());
                                    }

                                    // Add Messages listener
                                    ReportMessagesListeners.put(thisReport.getReportId(), dc.getDocument().getReference().
                                    collection("messages").limitToLast(1).orderBy("date")
                                    .addSnapshotListener((queryDocumentSnapshots, e1) -> {
                                                if (e1 != null) {
                                                    Log.d(TAG, e1.getMessage());
                                                    return;
                                                }

                                                for (DocumentChange messageEntryDocument : queryDocumentSnapshots.getDocumentChanges()) {
                                                    if (messageEntryDocument.getType() == DocumentChange.Type.ADDED) {

                                                        Message thisMessage = messageEntryDocument.getDocument().toObject(Message.class);

                                                        String latestMessage = " sent an attachment.";

                                                        if (thisMessage.isIs_media() == false) {
                                                            latestMessage = thisMessage.getMessage();
                                                        }

                                                        if (thisMessage.isInitial())
                                                            latestMessage = Jsoup.parse(thisMessage.getMessage()).text();

                                                        thisMessage.setMessage(latestMessage);
                                                        reportsAdapter.lastMessages.put(thisReport.getReportId(), thisMessage);

                                                        reportsAdapter.notifyDataSetChanged();
                                                    }

                                                }
                                            }
                                    ));

                                }


                                break;
                            case MODIFIED:

                                if(thisReport.getSenders().contains(AuthActivity.instance.loggedStudent.getEmail())) {
                                    if (index > -1) {
                                        reportList.set(index, thisReport);
                                        Log.d(TAG, thisReport.key + " modified : status = " + thisReport.getStatus());
//                                        if(mainActivity.viewedNotifications.contains(thisReport.key))
//                                        {
//                                            mainActivity.viewedNotifications.remove(thisReport.key);
//                                            Log.d(TAG, "Removed notification " + thisReport.reportId);
//                                            mainActivity.savePreference();
//                                        }
                                    }
                                    else
                                        reportList.add(thisReport);




                                    reportsAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    if(index > -1)
                                        reportList.remove(index);
                                    Log.d(TAG, "Im not included in this report: " + thisReport.reportId);
                                }

                                break;
                            case REMOVED:

                                Log.d(TAG, "Removed report: " + dc.getDocument().getData());

//                                Report thisReportRemoved = dc.getDocument().toObject(Report.class);
//                                int indexOfRemoved = getReportIndex(thisReport);

                                if(index > -1)
                                    reportList.remove(index);

//                                if(mainActivity.viewedNotifications.contains(thisReport.key))
//                                {
//                                    mainActivity.viewedNotifications.remove(thisReport.key);
//                                    Log.d(TAG, "Removed notification " + thisReport.reportId);
//                                    mainActivity.savePreference();
//                                }

                                break;
                        }
                    }

                    if(reportList.size() > 0)
                    {
                        Collections.sort(reportList, new Comparator<Report>() {
                            @Override
                            public int compare(final Report object1, final Report object2) {
                                return object2.getReportId().compareTo(object1.getReportId());
                            }
                        });
                    }

                    Log.d(TAG, "done loading my reports");
                    progressBar(false);
                    mSwipeRefreshLayout.setRefreshing(false);
                    reportsAdapter.notifyDataSetChanged();


                });
    }

    private int getReportIndex(Report report)
    {
        for (int i = 0; i < reportList.size(); i++) {
            if(report.reportId.equals(reportList.get(i).reportId))
                return i;
        }

        return -1;
    }

    private boolean hasReportAlready(Report report)
    {
        for (Report r : reportList) {
            if(r.reportId.equals(report.reportId))
                return true;
        };

        return false;
    }


    private void progressBar(boolean show) {
        View support_layout = view.findViewById(R.id.support_layout);

        if (show == false) {
            support_layout.setVisibility(View.GONE);
        } else {
            support_layout.setVisibility(View.VISIBLE);
        }
    }

    private String ListToString(List<String> list, String separator)
    {
        String result = "";
        for (int i=0; i < list.size(); i++)
        {
            if(i == 0)
                result = list.get(0);
            else
                result += separator  + list.get(i);
        }

        return result;
    }

    private List<String> StringToList(String str, String separator)
    {
        String[] splitted = str.split(separator);
        List<String> returnList = new ArrayList<>();//new ArrayList<String>(report.child("categories").getValue(String.class).split(","));
        for (int i=0; i < splitted.length; i++)
        {
            returnList.add(splitted[i]);
        }

        return  returnList;
    }

    private void postRun() {
        handler = new Handler();
        int delay = 5000;

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //dataLoader();
                //getMyReports();

                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        fetchMyReports();
    }

    public void preventClicks(View view) {}
}
