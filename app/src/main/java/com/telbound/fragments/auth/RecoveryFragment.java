package com.telbound.fragments.auth;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.telbound.orca.AuthActivity;
import com.telbound.orca.R;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecoveryFragment extends Fragment implements View.OnClickListener {
    private View view;

    private EditText account_id;

    private LinearLayout forgot_layout;
    private Animation shakeAnimation;
    private View support_layout;

    public RecoveryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_recovery, container, false);

        initViews();

        return view;
    }

    private void initViews() {
        FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();

        account_id = view.findViewById(R.id.forgot_account_id);
        Button forgot_submit = view.findViewById(R.id.auth_btn_forgot_submit);
        TextView forgot_login = view.findViewById(R.id.auth_link_login);

        forgot_submit.setOnClickListener( this );
        forgot_login.setOnClickListener( this );

        forgot_layout = view.findViewById(R.id.forgot_layout);
        shakeAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);

        support_layout = view.findViewById(R.id.support_layout);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.auth_btn_forgot_submit:
                checkValidation();
                break;
            case R.id.auth_link_login:
                new AuthActivity().replaceLoginFragment();
                break;
        }
    }

    private void progressBar(boolean show) {
        View support_layout = view.findViewById(R.id.support_layout);

        if (show == false) {
            support_layout.setVisibility(View.GONE);
        } else {
            support_layout.setVisibility(View.VISIBLE);
        }
    }

    private void checkValidation() {
        progressBar(false);
        
        TextInputLayout layout_account_id;
        String txt_account_id;

        txt_account_id = account_id.getText().toString();

        layout_account_id = view.findViewById(R.id.forgot_layout_account_id);

        if (txt_account_id.equals("") || txt_account_id.length() == 0) {
            layout_account_id.setErrorEnabled(true);
            layout_account_id.setError(getString(R.string.forgot_account_id_required));

            forgot_layout.startAnimation(shakeAnimation);
        } else {
            layout_account_id.setErrorEnabled(false);
            layout_account_id.setError(null);

            // Do http recovery request


            progressBar(true);

            FirebaseAuth auth = FirebaseAuth.getInstance();
            String emailAddress = txt_account_id;

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            auth.sendPasswordResetEmail(emailAddress)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {


                            builder.setMessage("We've sent an email to " + emailAddress + " to reset your password")
                                    .setTitle("Account recovery");
                            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog dialog = builder.create();

                            dialog.show();

                            progressBar(false);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            builder.setMessage(e.getMessage())
                                    .setTitle("Account recovery Failed!");
                            builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog dialog = builder.create();

                            dialog.show();

                            progressBar(false);
                        }
                    });
        }
    }
}
