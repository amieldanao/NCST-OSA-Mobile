package com.telbound.fragments.auth;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.telbound.models.Student;
import com.telbound.orca.AppController;
import com.telbound.orca.AuthActivity;
import com.telbound.orca.MainActivity;
import com.telbound.orca.R;
import com.telbound.utils.UtilConstants;
import com.telbound.utils.UtilFragments;
import com.telbound.utils.UtilSession;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    //private static final String TAG = LoginFragment.class.getSimpleName();
    private static final String TAG = "myLogTag";
    private View view;
    private View support_layout;

    private EditText account_id, password;

    private LinearLayout login_layout;
    private Animation shakeAnimation;
    private FragmentManager fragmentManager;
    private FirebaseFirestore db;

    public LoginFragment() {
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);

        AuthActivity.instance.profilePic = null;
        FirebaseAuth.getInstance().signOut();
        initViews();


        return view;
    }

    private void initViews() {
        fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        account_id = view.findViewById(R.id.login_account_id);
        password = view.findViewById(R.id.login_password);
        Button login_submit = view.findViewById(R.id.auth_btn_submit);
        TextView forgot_password = view.findViewById(R.id.auth_link_recovery);

        login_submit.setOnClickListener( this );
        forgot_password.setOnClickListener( this );
        support_layout = view.findViewById(R.id.support_layout);

//        account_id.setText("");
//        password.setText("");


        login_layout = view.findViewById(R.id.login_layout);
        shakeAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.auth_btn_submit:
                checkValidation();
                break;
            case R.id.auth_link_recovery:
                fragmentManager.beginTransaction().setCustomAnimations(R.anim.right_enter, R.anim.left_exit)
                    .replace(R.id.frameContainer, new RecoveryFragment(),
                        UtilFragments.RecoveryFragment).commit();
                break;
        }
    }





    private void checkValidation() {
        TextInputLayout layout_account_id, layout_password;
        String txt_account_id, txt_password;

        txt_account_id = account_id.getText().toString();
        txt_password = password.getText().toString();

        layout_account_id = view.findViewById(R.id.login_layout_account_id);
        layout_password = view.findViewById(R.id.login_layout_password);

        if (
                txt_account_id.equals("") || txt_account_id.length() == 0
                || txt_password.equals("") || txt_password.length() == 0
        ) {


            layout_account_id.setErrorEnabled(true);
            layout_password.setErrorEnabled(true);
            layout_account_id.setError(getString(R.string.login_account_id_required));
            layout_password.setError(getString(R.string.login_password_required));

            login_layout.startAnimation(shakeAnimation);
        } else {
            layout_account_id.setErrorEnabled(false);
            layout_password.setErrorEnabled(false);
            layout_account_id.setError(null);
            layout_password.setError(null);

            tryLogin(txt_account_id, txt_password);
        }
    }



    private void tryLogin(String account_id, String password)
    {
        progressBar(true);

        db.collection("students")
        .whereEqualTo("ID", account_id)
        .get()
        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                if(queryDocumentSnapshots.size() > 0) {

                    Student student = queryDocumentSnapshots.getDocuments().get(0).toObject(Student.class);

                    if(student.getStatus().equals("Active")) {

                        signInFirebase(student, password, queryDocumentSnapshots.getDocuments().get(0).getId());
                    }
                    else
                    {
                        progressBar(false);
                        if(student.getPassword().equals(password))
                            showToast("Your account has been " + student.getStatus());
                        else
                            showToast("Wrong Account ID or Password!");
                    }

                }
                else
                {
                    showToast("User doesn't exists!");
                    progressBar(false);
                }
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
//                if(!e.getMessage().contains("PERMISSION")) {
//                    showToast(e.getMessage());
//                }

                showToast(e.getMessage());
                progressBar(false);
            }
        });
    }

    private void showToast(String text)
    {
        Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
    }

    private void signInFirebase(Student student, String password, String key)
    {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(student.getEmail(), password).addOnSuccessListener(authResult -> {

            Map<String, Object> data = new HashMap<>();
            data.put("password", password);

            db.collection("students").document(key)
            .set(data, SetOptions.merge())
            .addOnSuccessListener(aVoid -> {
                AuthActivity.instance.loggedStudent = student;
                navigateToMain();
            })
            .addOnFailureListener(e -> {
                Log.w(TAG, "Error adding document", e);
                progressBar(false);
                showToast(e.getMessage());
                AuthActivity.instance.loggedStudent = null;
            });

        }).addOnFailureListener(e -> {
            progressBar(false);
            showToast(e.getMessage());
            AuthActivity.instance.loggedStudent = null;

        });
    }

    private void navigateToMain() {
        Intent i = new Intent(getContext(), MainActivity.class);

        startActivity(i);
        Objects.requireNonNull(getActivity()).finish();
    }

    private void progressBar(boolean show) {


        if (show) {
            support_layout.setVisibility(View.VISIBLE);
        } else {
            support_layout.setVisibility(View.GONE);
        }
    }
}
